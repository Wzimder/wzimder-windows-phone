﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Input;
using Wzimder.API.Adapter;
using Wzimder.API.Adapter.Models;
using Wzimder.Model.Types;
using Wzimder.Shared.ViewModel;
using System.Linq;

namespace Wzimder.ViewModel
{
    public class ZarejestrujViewModel : ViewModelBase
    {
        //Fields
        /// <summary>
        /// Obiekt przechowujący dane z widoku. pobiera dane z bindowania.
        /// </summary>
        private User uzytkownik = new User();

        private Wzimder.Shared.ViewModel.ViewModelLocator serviceLocator;
        private ServerAdapter serverAdapter;
        private INavigationService navigationService;
        private User user;
        private Gender gender;
        private RegisterRequest registerM;
        private string email;
        private string password;
        private string confirmPassword;
        private string firstName;
        private string lastName;
        private int genderView;
        private DateTime birthDate;
        private string description;
        private int genderButton;
        private bool nameError;
        private bool lastNameError;
        private bool emailError;
        private bool passwordError;
        private bool confirmPasswordError;
        private bool descriptionError;

        //Properties
        public ViewModelLocator ViewModelLocator { get { return ViewModelLocator; } }
        public ServerAdapter ServerAdapter { get { return serverAdapter; } }
        public User User { get { return user; } set { user = value; } }
        public Gender Gender { get { return gender; } set { gender = value; } }
        public INavigationService NavigationService { get { return navigationService; } }
        public RegisterRequest RegisterM
        {
            get { return registerM; }
            set
            {
                registerM = value;
            }
        }
        public string FirstName { get { return firstName; } set { firstName = value; RegisterCommand.RaiseCanExecuteChanged(); RaisePropertyChanged("FirstName"); } }
        public string Email { get { return email; } set { email = value; RegisterCommand.RaiseCanExecuteChanged(); RaisePropertyChanged("Email"); } }
        public string Password { get { return password; } set { password = value; RegisterCommand.RaiseCanExecuteChanged(); RaisePropertyChanged("Password"); } }
        public string ConfirmPassword { get { return confirmPassword; } set { confirmPassword = value; RegisterCommand.RaiseCanExecuteChanged(); RaisePropertyChanged("ConfirmPassword"); } }
        public string LastName { get { return lastName; } set { lastName = value; RegisterCommand.RaiseCanExecuteChanged(); RaisePropertyChanged("LastName"); } }
        public int GenderView { get { return genderView; } set { genderView = value; RegisterCommand.RaiseCanExecuteChanged(); RaisePropertyChanged("GenderView"); } }
        public DateTime BirthDate {
            get {
                return birthDate;
            }
            set {
                birthDate = value;
                RegisterCommand.RaiseCanExecuteChanged();
                RaisePropertyChanged("BirthDate");
            }
        }
        public string Description { get { return description; } set { description = value; RegisterCommand.RaiseCanExecuteChanged(); RaisePropertyChanged("Description"); } }
        public int GenderButton
        {
            get { return genderButton; }
            set
            {
                genderButton = value;
                RegisterCommand.RaiseCanExecuteChanged();
                RaisePropertyChanged("GenderButton");
            }
        }
        public bool NameError { get { return nameError; } set { nameError = value; RaisePropertyChanged("NameError"); } }
        public bool LastNameError { get { return lastNameError; } set { lastNameError = value; RaisePropertyChanged("LastNameError"); } }
        public bool EmailError { get { return emailError; } set { emailError = value; RaisePropertyChanged("EmailError"); } }
        public bool PasswordError { get { return passwordError; } set { passwordError = value; RaisePropertyChanged("PasswordError"); } }
        public bool ConfirmPasswordError { get { return confirmPasswordError; } set { confirmPasswordError = value; RaisePropertyChanged("ConfirmPasswordError"); } }
        public bool DescriptionError { get { return descriptionError; } set { descriptionError = value; RaisePropertyChanged("DescriptionError"); } }
        //Properties for View
        /// <summary>
        /// Obiekt przechowujący dane z widoku. pobiera dane z bindowania.
        /// </summary>
        public User Uzytkownik { get { return user; } set { user = value; RaisePropertyChanged("Uzytkownik"); } }

        //Constructors
        public ZarejestrujViewModel(INavigationService navigationService)
        {
            this.navigationService = navigationService;
            this.serviceLocator = App.Current.Resources["Locator"] as Wzimder.Shared.ViewModel.ViewModelLocator;
            this.serverAdapter = this.serviceLocator.ServerAdapter;
            user = new Model.Types.User();
            gender = new Wzimder.Model.Types.Gender();
            this.registerM = new RegisterRequest();
            this.RegisterCommand = new RelayCommand(register, canExecuteRegisterCommand);
            this.changeDate = new RelayCommand<object>((date) => selectDate(date));
        }

        //Methods
        private void register()
        {
            Wzimder.Model.Types.Gender genderTmp;
            if (GenderButton == 0)
            {
                genderTmp = Wzimder.Model.Types.Gender.Male;
            }
            else
            {
                genderTmp = Wzimder.Model.Types.Gender.Female;
            }

            RegisterRequest reg = new RegisterRequest()
            {
                Email = Email,
                BirthDate = BirthDate,
                ConfirmPassword = ConfirmPassword,
                Description = Description,
                FirstName = FirstName,
                LastName = LastName,
                Password = Password,
                Gender = genderTmp
            };

            bool isValid = false;

            try
            {
                isValid = serverAdapter.AccountApiService.Register(reg);
            }
            catch (Exception ex)
            {
                isValid = false;
               // throw ex;
            }

            if (isValid)
            {
                this.NavigationService.NavigateTo("LogowaniePage");
            }
            else
            {
                this.NavigationService.NavigateTo("ZarejestrujPage");
            }
        }

        public RelayCommand RegisterCommand
        {
            get;
            internal set;
        }

        public RelayCommand<object> changeDate
        {
            get;
            internal set;
        }

        private bool canExecuteRegisterCommand()
        {
            return firstNameVaid(FirstName) && lastNameVaid(LastName) && emailIsValid(Email) && passwordIsValid(Password) && confirmPasswordIsValid(Password, ConfirmPassword) 
                && descriptionValid(Description) &&  BirthDate != null ;
        }

        private bool firstNameVaid(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                NameError = true;
                return false;
            }

            foreach (var item in name)
            {
                if (!char.IsLetter(item))
                {
                    NameError = true;
                    return false;
                }
            }
            NameError = false;
            return true;
        }

        private bool lastNameVaid(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                LastNameError = true;
                return false;
            }

            foreach (var item in name)
            {
                if (!char.IsLetter(item))
                {
                    LastNameError = true;
                    return false;
                }
            }
            LastNameError = false;
            return true;
        }

        private bool emailIsValid(string email)
        {
            if (string.IsNullOrEmpty(Email))
            {
                EmailError = true;
                return false;
            }


            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = regex.Match(email);
            if (match.Success)
            {
                EmailError = false;
                return true;
            }
            else
            {
                EmailError = true;
                return false;
            }

        }

        private bool descriptionValid(string description)
        {
            if (string.IsNullOrEmpty(description))
            {
                DescriptionError = true;
                return false;
            }
            DescriptionError = false;
            return true;
        }

        private bool passwordIsValid(string password)
        {
            if(string.IsNullOrEmpty(password))
            {
                PasswordError = true;
                return false;
            }

            if (password.Length < 6)
            {
                PasswordError = true;
                return false;
            }

            if (password.Length > 100)
            {
                PasswordError = true;
                return false;
            }

            bool isUpperChar = false;

            foreach (var letter in password)
            {
                if (char.IsLetter(letter) && char.IsUpper(letter))
                    isUpperChar = true;
            }

            if (isUpperChar == false)
            {
                PasswordError = true;
                return false;
            }

            bool isLowerChar = false;

            foreach (var letter in password)
            {
                if (char.IsLetter(letter) && char.IsLower(letter))
                    isLowerChar = true;
            }

            if (isLowerChar == false)
            {
                PasswordError = true;
                return false;
            }

            char[] forbiddenChars = { '(', ')', ' ' };

            foreach (var letter in password)
            {
                if (forbiddenChars.Contains(letter))
                {
                    PasswordError = true;
                    return false;
                }
            }

            char[] specialChar = { '_', '@', '$', '%', '^', '!', '*', '.', '"', '#', '&' };

            bool isSpecialChar = false;

            foreach (var letter in password)
            {
                if (specialChar.Contains(letter))
                    isSpecialChar = true;
            }

            if (isSpecialChar == false)
            {
                PasswordError = true;
                return false;
            }
            PasswordError = false;
            return true;
        }

        private bool confirmPasswordIsValid(string password, string confirmPassword)
        {
            if (password != confirmPassword)
            {
                ConfirmPasswordError = true;
                return false;
            }

            ConfirmPasswordError = false;

            return true;
        }

        private void selectDate(object d)
        { }
    }
}
