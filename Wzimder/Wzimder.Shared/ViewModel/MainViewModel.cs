using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using System.Collections.ObjectModel;
using System;

namespace Wzimder.Shared.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainMenuViewModel : ViewModelBase
    {
        //Fields
        private INavigationService navigationService;
        public ViewModelLocator ViewModelLocator { get { return App.Current.Resources["Locator"] as ViewModelLocator; } }
        private ObservableCollection<string> options = new ObservableCollection<string>()
        {
            "Strona G��wna", "Wyszukaj", "Chat", "Twoje Pary", "Tw�j Profil", "Wyloguj", "Wyniki Zaakceptowane"
        };

        //Properties
        public INavigationService NavigationService { get { return navigationService; } }
        public ObservableCollection<string> Options { get { return options; } }

        //public RelayCommand DetailsCommand { get; set; }

        //Constructors
        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        //public MainViewModel()
        //{
        //    ////if (IsInDesignMode)
        //    ////{
        //    ////    // Code runs in Blend --> create design time data.
        //    ////}
        //    ////else
        //    ////{
        //    ////    // Code runs "for real"
        //    ////}
        //}
        public MainMenuViewModel(INavigationService navigationService)
        {
            this.navigationService = navigationService;

            //DetailsCommand = new RelayCommand(() =>
            //{
            //    navigationService.NavigateTo("Details", "My data");
            //});
        }

        //Methods
        /// <summary>
        /// Metoda jest wywo�ywana, gdy zostanie wybrana opcja w Main Menu.
        /// </summary>
        public void ItemSelected(string selectedValue)
        {
            switch (selectedValue)
            {
                case "Strona G��wna":
                    StronaGlownaSelected();
                    break;
                case "Wyszukaj":
                    WyszukajSelected();
                    break;
                case "Chat":
                    ChatSelected();
                    break;
                case "Twoje Pary":
                    TwojeParySelected();
                    break;
                case "Tw�j Profil":
                    TwojProfilSelected();
                    break;
                case "Wyloguj":
                    WylogujSelected();
                    break;
                case "Wyniki Zaakceptowane":
                    WynikiZaakceptowaneSelected();
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Metoda wyw�ana jest wtedy, gdy zostanie nacisni�ty przycisk Wyniki Zaakceptowane w MainMenu.
        /// </summary>
        private void WynikiZaakceptowaneSelected()
        {
            bool userIsLoggedIn = ViewModelLocator.LogowanieViewModel.LastLoginResponse != null;

            if (userIsLoggedIn)
            {
                this.navigationService.NavigateTo("WynikiZaakceptowanePage");
            }
            else
            {
                this.navigationService.NavigateTo("LogowaniePage");
            }
        }

        /// <summary>
        /// Metoda wyw�ana jest wtedy, gdy zostanie nacisni�ty przycisk Strona G��wna w MainMenu.
        /// </summary>
        private void StronaGlownaSelected()
        {
            bool userIsLoggedIn = ViewModelLocator.LogowanieViewModel.LastLoginResponse != null;

            if (userIsLoggedIn)
            {
                this.navigationService.NavigateTo("WynikiPage");
            }
            else
            {
                this.navigationService.NavigateTo("LogowaniePage");
            }
        }

        /// <summary>
        /// Metoda wyw�ana jest wtedy, gdy zostanie nacisni�ty przycisk Wyszukaj w MainMenu.
        /// </summary>
        private void WyszukajSelected()
        {
            bool userIsLoggedIn = ViewModelLocator.LogowanieViewModel.LastLoginResponse != null;

            if (userIsLoggedIn)
            {
                this.navigationService.NavigateTo("WyszukiwaniePage");
            }
            else
            {
                this.navigationService.NavigateTo("LogowaniePage");
            }
        }

        /// <summary>
        /// Metoda wyw�ana jest wtedy, gdy zostanie nacisni�ty przycisk Chat w MainMenu.
        /// </summary>
        private void ChatSelected()
        {
            bool userIsLoggedIn = ViewModelLocator.LogowanieViewModel.LastLoginResponse != null;

            if (userIsLoggedIn)
            {
                this.navigationService.NavigateTo("CzatPage");
            }
            else
            {
                this.navigationService.NavigateTo("LogowaniePage");
            }
        }

        /// <summary>
        /// Metoda wyw�ana jest wtedy, gdy zostanie nacisni�ty przycisk Twoje Pary w MainMenu.
        /// </summary>
        private void TwojeParySelected()
        {
            bool userIsLoggedIn = ViewModelLocator.LogowanieViewModel.LastLoginResponse != null;

            if (userIsLoggedIn)
            {
                this.navigationService.NavigateTo("WynikiPage");
            }
            else
            {
                this.navigationService.NavigateTo("LogowaniePage");
            }
        }

        /// <summary>
        /// Metoda wyw�ana jest wtedy, gdy zostanie nacisni�ty przycisk Tw�j Profil w MainMenu.
        /// </summary>
        private void TwojProfilSelected()
        {
            bool userIsLoggedIn = ViewModelLocator.LogowanieViewModel.LastLoginResponse != null;

            if (userIsLoggedIn)
            {
                this.navigationService.NavigateTo("EdytujPage");
            }
            else
            {
                this.navigationService.NavigateTo("LogowaniePage");
            }
        }

        /// <summary>
        /// Metoda wyw�ana jest wtedy, gdy zostanie nacisni�ty przycisk Wyloguj w MainMenu.
        /// </summary>
        private void WylogujSelected()
        {
            ViewModelLocator.CzatViewModel.ClearCzat();
            ViewModelLocator.LogowanieViewModel.Wyloguj();
            this.navigationService.NavigateTo("LogowaniePage");
        }
    }
}