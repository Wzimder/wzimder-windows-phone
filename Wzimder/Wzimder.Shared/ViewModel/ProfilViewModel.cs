﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using GalaSoft.MvvmLight.Views;
using System;
using System.Collections.Generic;
using System.Text;
using Wzimder.API.Adapter;
using Wzimder.API.Adapter.Models;
using Wzimder.Model.Types;

namespace Wzimder.Shared.ViewModel
{
    public class ProfilViewModel : ViewModelBase
    {
        //Fields
        private ViewModelLocator serviceLocator;
        private ServerAdapter serverAdapter;
        private INavigationService navigationService;
        private string userId;
        private User userObject;

        //Properties
        public ViewModelLocator ViewModelLocator { get { return this.serviceLocator; } }
        public ServerAdapter ServerAdapter { get { return serverAdapter; } }
        public INavigationService NavigationService { get { return navigationService; } }
        public string UserId { get { return userId; } set { userId = value; } }
        public User UserObject { get { return userObject; } set { userObject = value; } }
        public LoginResponse LastLoginResponse { get { return this.ViewModelLocator?.LogowanieViewModel?.LastLoginResponse; } }
        public int Like { get { return 0; } }
        public int UnLike { get { return 1; } }
        public string Odleglosc
        {
            get
            {
                switch (this.serviceLocator.WyszukiwanieViewModel.SelectedDistance)
                {
                    case 0: return "0-5 km";
                    case 1: return "5-15 km";
                    case 2: return "15-100 km";
                }

                return "Brak danych";      
            }
        }

        //Constructors
        public ProfilViewModel(INavigationService navigationService)
        {
            //Inicjalizacja
            this.navigationService = navigationService;
            this.serviceLocator = App.Current.Resources["Locator"] as ViewModelLocator;
            this.serverAdapter = this.serviceLocator.ServerAdapter;
            this.AddUserCommand = new RelayCommand(addUser);
            this.RemoveUserCommand = new RelayCommand(removeUser);
            UserObject = this.serviceLocator.WynikiViewModel.UserId;

            if (UserObject == null)
            {
                throw new Exception("Nie można rozpoznać id użytkownika dla którego mają byc probrane dane.");
            }
            else
            {
                this.UserObject = serverAdapter.UsersApiService.GetUsers(ViewModelLocator.LogowanieViewModel.LastLoginResponse, UserObject.Id);
            }
        }

        //Methods
        public RelayCommand AddUserCommand { get;internal set;}
        public RelayCommand RemoveUserCommand { get; internal set; }

        private void addUser()
        {
            this.serverAdapter.PairsApiService.SendPairs(LastLoginResponse, new Pair() { FirstUserId = UserObject.Id, FirstUserStatus = UserStatus.Like });
            this.NavigationService.NavigateTo("WynikiPage");
        }

        private void removeUser()
        {
            this.serverAdapter.PairsApiService.SendPairs(LastLoginResponse, new Pair() { FirstUserId = UserObject.Id, FirstUserStatus = UserStatus.DontLike });
            this.NavigationService.NavigateTo("WynikiPage");
        }
    }
}
