﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using System;
using System.Collections.Generic;
using System.Text;
using Wzimder.API.Adapter;
using Wzimder.API.Adapter.Models;
using Wzimder.Model.Types;

namespace Wzimder.ViewModel
{
    public class EdytujViewModel : ViewModelBase
    {
        //Fields
        private Shared.ViewModel.ViewModelLocator serviceLocator;
        private ServerAdapter serverAdapter;
        private INavigationService navigationService;
        private string zrodloObrazka = "../Assets/avatar.jpg";
        
        //Properties
        public Wzimder.Shared.ViewModel.ViewModelLocator ViewModelLocator { get { return serviceLocator; } }
        public ServerAdapter ServerAdapter { get { return serverAdapter; } }
        public INavigationService NavigationService { get { return navigationService; } }
        public string NoweHaslo { get; set; }
        public string PowtorzHaslo { get; set; }

        public string ZrodloObrazka { get { return zrodloObrazka; } set { zrodloObrazka = value; RaisePropertyChanged("ZrodloObrazka"); } }

        //Commands
        public RelayCommand ChangePasswordCommand { get; internal set; }
        public RelayCommand ChooseImageCommand { get; internal set; }

        //Constructors
        public EdytujViewModel(INavigationService navigationService)
        {
            this.navigationService = navigationService;
            this.serviceLocator = App.Current.Resources["Locator"] as Wzimder.Shared.ViewModel.ViewModelLocator;
            this.serverAdapter = this.serviceLocator.ServerAdapter;
            this.ChangePasswordCommand = new RelayCommand(changePassword);
            this.ChooseImageCommand = new RelayCommand(DownloadImage);

            DownloadImage();
        }

        //Methods
        /// <summary>
        /// Zmienia hasło dla zalogowanego konta.
        /// </summary>
        private void changePassword()
        {
            //Tutaj dodać walidację hasła

            ChangePasswordRequest changePassRequest = new ChangePasswordRequest()
            {
                OldPassword = ViewModelLocator.LogowanieViewModel.UserPassword,
                NewPassword = this.NoweHaslo,
                ConfirmPassword = this.PowtorzHaslo,
            };

            bool isChange = serverAdapter.AccountApiService.ChangePassword(this.ViewModelLocator.LogowanieViewModel.LastLoginResponse, changePassRequest);

            if (isChange)
            {
                //Wyswietlenie wiadomości o powodzeniu zmiany hasła. (event TODO)
            }
            else
            {
                //Wyswietlenie informacji o niepowodzeniu zmiany hasła (event TODO)
            }
        }

        /// <summary>
        /// Ściąga obrazek dla danej osoby.
        /// </summary>
        public void DownloadImage()
        {
            User user = this.serviceLocator.ServerAdapter.AccountApiService.GetUser(this.serviceLocator.LogowanieViewModel.LastLoginResponse);
            this.serviceLocator.LogowanieViewModel.User.ProfilePhotoId = user.ProfilePhotoId;
            this.ZrodloObrazka = this.serviceLocator.LogowanieViewModel.User.PhotoLink;
        }
    }
}
