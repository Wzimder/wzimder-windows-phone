﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Wzimder.API.Adapter;
using Wzimder.API.Adapter.Models;
using Wzimder.Model.Types;

namespace Wzimder.Shared.ViewModel
{
    public class CzatViewModel : ViewModelBase
    {   
        //Fields
        private ViewModelLocator serviceLocator;
        private ServerAdapter serverAdapter;
        private INavigationService navigationService;
        private ObservableCollection<Message> messages = new ObservableCollection<Message>() { };
        DispatcherTimer czatTimerUpdater;

        //Properties
        public ViewModelLocator ViewModelLocator { get { return ViewModelLocator; } }
        public ServerAdapter ServerAdapter { get { return serverAdapter; } }
        public INavigationService NavigationService { get { return navigationService; } }

        public User SelectedUser { get { return this.serviceLocator.WynikiZaakceptowaneViewModel.UserId;  } }
        public ObservableCollection<Message> Messages { get { return messages; } set { this.messages = value; } }
        public string MessageText { get; set; }
        public LoginResponse LastLoginResponse { get { return this.serviceLocator.LogowanieViewModel.LastLoginResponse; } }

        //Commands
        public RelayCommand SendMessageCommand { get; set; }

        //Constructors
        public CzatViewModel(INavigationService navigationService)
        {
            this.navigationService = navigationService;
            this.serviceLocator = App.Current.Resources["Locator"] as ViewModelLocator;
            this.serverAdapter = this.serviceLocator.ServerAdapter;

            this.SendMessageCommand = new RelayCommand(SendMessage);

            this.czatTimerUpdater = new DispatcherTimer();
            this.czatTimerUpdater.Interval = new TimeSpan(seconds: 10, minutes: 0, hours: 0);
            this.czatTimerUpdater.Tick += CzatTimerUpdater_Tick;
        }

        //Methods
        private void CzatTimerUpdater_Tick(object sender, object e)
        {
            UpdateCzat();
        }
        public void EnableCzatUpdate()
        {
            this.czatTimerUpdater.Start();
        }
        public void DisableCzatUpdate()
        {
            this.czatTimerUpdater.Stop();
        }
        public void UpdateCzat()
        {
            List<Message> messages = this.serverAdapter.MessagesApiService.GetMessages(LastLoginResponse);
            foreach (Message message in messages)
            {
                message.Sender = SelectedUser;
                this.Messages.Add(message);
            }
        }
        public void SendMessage()
        {
            this.ServerAdapter.MessagesApiService.SendMessages(LastLoginResponse, this.MessageText, SelectedUser.Id);

            this.Messages.Add(new Message()
            {
                Sender = this.serviceLocator.LogowanieViewModel.User,
                Text = MessageText,
            });
        }

        public void ClearCzat()
        {
            this.messages = new ObservableCollection<Message>();
            this.MessageText = string.Empty;
            DisableCzatUpdate();
        }
    }


}
