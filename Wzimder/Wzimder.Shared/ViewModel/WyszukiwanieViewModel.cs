﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.Devices.Geolocation;
using Windows.UI.Popups;
using Wzimder.API.Adapter;
using Wzimder.API.Adapter.Models;
using Wzimder.Model.Types;
using Wzimder.ValueConverter;

namespace Wzimder.Shared.ViewModel
{
    public class WyszukiwanieViewModel : ViewModelBase
    {
        //Fields
        private ViewModelLocator serviceLocator;
        private ServerAdapter serverAdapter;
        private INavigationService navigationService;
        private Wzimder.Model.Types.SearchOption options;
        private Geolocator locator = null;
        double latitude;
        double longitude;
        private int genderButton;
        private int? selectedAge;
        private int? selectedDistance;

        //Properties
        public ViewModelLocator ViewModelLocator { get { return serviceLocator; } }
        public ServerAdapter ServerAdapter { get { return serverAdapter; } }
        public SearchOption Options { get { return options; } set { options = value; } }
        public INavigationService NavigationService { get { return navigationService; } }
        public Geolocation ActualGeolocation { get; set; }
        public int GenderButton { get { return genderButton; }
            set
            {
                genderButton = value;
                OptionsSetCommand.RaiseCanExecuteChanged();
                RaisePropertyChanged("GenderButton");
            }
        }
        public int? SelectedAge {
            get { return selectedAge; }
            set
            {
                selectedAge = value;
                OptionsSetCommand.RaiseCanExecuteChanged();
                RaisePropertyChanged("SelectedAge");
            }
        }
        public int? SelectedDistance
        {
            get { return selectedDistance; }
            set
            {
                selectedDistance = value;
                OptionsSetCommand.RaiseCanExecuteChanged();
                RaisePropertyChanged("SelectedDistance");
            }
        }

        //Constructors
        public WyszukiwanieViewModel(INavigationService navigationService)
        {
            this.navigationService = navigationService;
            this.serviceLocator = App.Current.Resources["Locator"] as ViewModelLocator;
            this.serverAdapter = this.serviceLocator.ServerAdapter;
            this.options = new Model.Types.SearchOption();
            this.OptionsSetCommand = new RelayCommand(optionsSet, canExecuteOptionsSetCommand);
            SetOptionsFromServer();
            getLocation();
        }

        //Methods
        public RelayCommand OptionsSetCommand
        {
            get;
            internal set;
        }

        private void optionsSet()
        {
            LoginResponse response = this.serviceLocator.LogowanieViewModel.LastLoginResponse;
            var isValid = serverAdapter.SearchOptionsApiService.SendSearchOptions(response, Options);
            if (isValid)
            {
                NavigationService.NavigateTo("WynikiPage");
            }
            else
            {
                NavigationService.NavigateTo("WynikiPage");
            }
        }

        private bool canExecuteOptionsSetCommand()
        {
            if (SelectedAge != null && SelectedDistance != null)
            {
                setOptions(ref options);
                return true;
            }
            else
                return false;
        }

        private void setOptions(ref SearchOption options)
        {
            
            setAge((int)SelectedAge,ref options);
            setDistance((int)selectedDistance, ref options);
            if (GenderButton == 0)
            {
                options.Gender = Wzimder.Model.Types.Gender.Male;
            }
            else
            {
                options.Gender = Wzimder.Model.Types.Gender.Female;
            }
            
            ActualGeolocation = new Geolocation();
            ActualGeolocation.Latitude = this.latitude;
            ActualGeolocation.Longitude = this.longitude;
            LoginResponse lastLoginResponse = this.serviceLocator.LogowanieViewModel.LastLoginResponse;
            serverAdapter.AccountApiService.Location(lastLoginResponse, ActualGeolocation);
        }

        private void setAge(int age, ref SearchOption options)
        {
            if (age == (int)ageEnum.age18to24)
            {
                options.MinAge = 18;
                options.MaxAge = 24;
            }
            if (age == (int)ageEnum.age25to30)
            {
                options.MinAge = 25;
                options.MaxAge = 30;
            }
            if (age == (int)ageEnum.age31to40)
            {
                options.MinAge = 31;
                options.MaxAge = 40;
            }
        }

        private void setDistance(int distance, ref SearchOption options)
        {
            if (distance == (int)distanceEnum.distance0to5)
            {
                options.MaxDistanceKM = 5;
            }
            if (distance == (int)distanceEnum.distance5to15)
            {
                options.MaxDistanceKM = 15;
            }
            if (distance == (int)distanceEnum.distance15to100)
            {
                options.MaxDistanceKM = 100;
            }
        }

        private void SetOptionsFromServer()
        {
            LoginResponse lastLoginResponse = this.serviceLocator.LogowanieViewModel.LastLoginResponse;
            var optionsFromServer = serverAdapter.SearchOptionsApiService.GetSearchOptions(lastLoginResponse);

            if (optionsFromServer != null)
            {
                this.options = optionsFromServer;
            }
        }

        async private void getLocation()
        {
            if (locator == null)
            {
                locator = new Geolocator();
            }
            if (locator != null)
            {
                if (isLocationOn())
                {
                    Geoposition c = await locator.GetGeopositionAsync();
                    Geocoordinate g = c.Coordinate;

                    latitude = g.Point.Position.Latitude;
                    longitude = g.Point.Position.Longitude;
                }
            }
        }

        private bool isLocationOn()
        {
            return locator.LocationStatus != PositionStatus.Disabled;
        }

        async private void showMessage(string message)
        {
            MessageDialog msgbox = new MessageDialog(message);
            await msgbox.ShowAsync();
        }

    }

    enum ageEnum
    {
        age18to24,
        age25to30,
        age31to40
    };

    enum distanceEnum
    {
        distance0to5,
        distance5to15,
        distance15to100
    };
}
