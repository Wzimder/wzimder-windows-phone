﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Wzimder.API.Adapter;
using Wzimder.API.Adapter.Models;
using Wzimder.Model.Types;
using Wzimder.Shared.ViewModel;

namespace Wzimder.ViewModel
{
    public class WynikiZaakceptowaneViewModel : ViewModelBase
    {
        //Fields
        private Shared.ViewModel.ViewModelLocator serviceLocator;
        private ServerAdapter serverAdapter;
        private INavigationService navigationService;
        private ObservableCollection<User> userList;
        private User userId;

        //Properties
        public Wzimder.Shared.ViewModel.ViewModelLocator ViewModelLocator { get { return serviceLocator; } }
        public ServerAdapter ServerAdapter { get { return serverAdapter; } }
        public INavigationService NavigationService { get { return navigationService; } }
        public ObservableCollection<User> UserList { get { return userList; } set { userList = value; } }
        public User UserId { get { return userId; } set { userId = value; } }
        public LoginResponse LastLoginResponse { get { return this.ViewModelLocator?.LogowanieViewModel?.LastLoginResponse; } }
        public RelayCommand<object> SelectUserCommand
        {
            get;
            internal set;
        }

        //Constructors
        public WynikiZaakceptowaneViewModel(INavigationService navigationService)
        {
            this.navigationService = navigationService;
            this.serviceLocator = App.Current.Resources["Locator"] as Shared.ViewModel.ViewModelLocator;
            this.serverAdapter = this.serviceLocator.ServerAdapter;
            this.SelectUserCommand = new RelayCommand<object>((id) => SelectUser(id));
        }

        //Methods
        public void UpdateUsers()
        {
            if (LastLoginResponse != null && LastLoginResponse.access_token != null)
            {
                List<User> userList = this.serverAdapter.PairsApiService.GetPairs(LastLoginResponse);
                this.UserList = new ObservableCollection<User>(userList);
            }
        }

        public void SelectUser(object parmeter)
        {
            UserId = (User)parmeter;
            this.NavigationService.NavigateTo("CzatPage");
        }
    }
}
