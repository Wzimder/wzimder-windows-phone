﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using GalaSoft.MvvmLight.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wzimder.API.Adapter;
using Wzimder.API.Adapter.Models;
using Wzimder.Model.Types;

namespace Wzimder.Shared.ViewModel
{
    public class WynikiViewModel : ViewModelBase
    {
        //Fields
        private ViewModelLocator serviceLocator;
        private ServerAdapter serverAdapter;
        private INavigationService navigationService;
        private ObservableCollection<User> userList = SampleUsersForDebug.Users;
        private User userId;



        //Properties
        public ViewModelLocator ViewModelLocator { get { return serviceLocator; } }
        public ServerAdapter ServerAdapter { get { return serverAdapter; } }
        public INavigationService NavigationService { get { return navigationService; } }
        public ObservableCollection<User> UserList { get { return userList; } set { userList = value; } }
        public User UserId { get { return userId; } set { userId = value; } }
        public LoginResponse LastLoginResponse { get { return this.ViewModelLocator?.LogowanieViewModel?.LastLoginResponse; } }
        public RelayCommand<object> SelectUserCommand
        {
            get;
            internal set;
        }

        //Constructors
        public WynikiViewModel(INavigationService navigationService)
        {
            this.navigationService = navigationService;
            this.serviceLocator = App.Current.Resources["Locator"] as ViewModelLocator;
            this.serverAdapter = this.serviceLocator.ServerAdapter;
            this.SelectUserCommand = new RelayCommand<object>((id) => SelectUser(id));
        }

        //Methods
        /// <summary>
        /// Aktualizacja listy użytkowników w widoku wyniku wyszukiwania.
        /// Ściągnij liste uzytkowników (max 30) i wyświetl ich w wynikach.
        /// </summary>
        public void UpdateUsers()
        {
            if (LastLoginResponse != null && LastLoginResponse.access_token != null)
            {
                List<User> userList = this.serverAdapter.UsersApiService.GetUsers(LastLoginResponse);
                this.UserList = new ObservableCollection<User>(userList);
            }
        }

        private void SelectUser(object parmeter)
        {
            UserId = (User)parmeter;
            this.NavigationService.NavigateTo("ProfilPage");
        }
    }

    public static class SampleUsersForDebug
    {
        public static ObservableCollection<User> Users { get
            {
                return new ObservableCollection<User>()
                {
                    new User() { Id = "0", FirstName = "DANE!!!", LastName = "TESTOWE!!!", BirthDate = new DateTime(1990, 05, 20), Gender = Gender.Male, ProfilePhotoId = 4,  },
                    new User() { Id = "1", FirstName = "Adam", LastName = "Nowak", BirthDate = new DateTime(1991, 05, 23), Gender = Gender.Male, ProfilePhotoId = 2,  },
                    new User() { Id = "2", FirstName = "Krzysztof", LastName = "Zieliński", BirthDate = new DateTime(1989, 02, 5), Gender = Gender.Male, ProfilePhotoId = 4,  },
                    new User() { Id = "3", FirstName = "Jan", LastName = "Kwiatkowski", BirthDate = new DateTime(1990, 09, 30), Gender = Gender.Male, ProfilePhotoId = 7,  },
                    new User() { Id = "4", FirstName = "Zenon", LastName = "Nowakowski", BirthDate = new DateTime(1990, 05, 20), Gender = Gender.Male, ProfilePhotoId = 2,  },
                };
            }
        }
    }
}
