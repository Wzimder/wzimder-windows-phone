/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocator xmlns:vm="clr-namespace:Wzimder"
                           x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"

  You can also use Blend to do all this with the tool's support.
  See http://www.galasoft.ch/mvvm
*/

using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Views;
using Microsoft.Practices.ServiceLocation;
using Wzimder.API.Adapter;
using Wzimder.Shared.ViewModel;
using Wzimder.ValueConverter;
using Wzimder.ViewModel;

namespace Wzimder.Shared.ViewModel
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// </summary>
    public class ViewModelLocator
    {
        //Properties
        public INavigationService NavigationService { get; set; }

        public ServerAdapter ServerAdapter { get { return SimpleIoc.Default.GetInstance<ServerAdapter>(); } }
        public MainMenuViewModel MainViewModel { get { return SimpleIoc.Default.GetInstance<MainMenuViewModel>(); } }
        public CzatViewModel CzatViewModel { get { return SimpleIoc.Default.GetInstance<CzatViewModel>(); } }
        public LogowanieViewModel LogowanieViewModel { get { return SimpleIoc.Default.GetInstance<LogowanieViewModel>(); } }
        public ProfilViewModel ProfilViewModel { get { return SimpleIoc.Default.GetInstance<ProfilViewModel>(); } }
        public WynikiViewModel WynikiViewModel { get { return SimpleIoc.Default.GetInstance<WynikiViewModel>(); } }
        public WyszukiwanieViewModel WyszukiwanieViewModel { get { return SimpleIoc.Default.GetInstance<WyszukiwanieViewModel>(); } }
        public ZarejestrujViewModel ZarejestrujViewModel { get { return SimpleIoc.Default.GetInstance<ZarejestrujViewModel>(); } }
        public EdytujViewModel EdytujViewModel { get { return SimpleIoc.Default.GetInstance<EdytujViewModel>(); } }
        public WynikiZaakceptowaneViewModel WynikiZaakceptowaneViewModel { get { return SimpleIoc.Default.GetInstance<WynikiZaakceptowaneViewModel>(); } }

        //Constructor
        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            ////if (ViewModelBase.IsInDesignModeStatic)
            ////{
            ////    // Create design time view services and models
            ////    SimpleIoc.Default.Register<IDataService, DesignDataService>();
            ////}
            ////else
            ////{
            ////    // Create run time view services and models
            ////    SimpleIoc.Default.Register<IDataService, DataService>();
            ////}

            //INavigation Service
            this.NavigationService = this.CreateNavigationService();
            SimpleIoc.Default.Register<INavigationService>(() => this.NavigationService);

            SimpleIoc.Default.Register<ServerAdapter>();
            SimpleIoc.Default.Register<MainMenuViewModel>();
            SimpleIoc.Default.Register<CzatViewModel>();
            SimpleIoc.Default.Register<LogowanieViewModel>();
            SimpleIoc.Default.Register<ProfilViewModel>();
            SimpleIoc.Default.Register<WynikiViewModel>();
            SimpleIoc.Default.Register<WyszukiwanieViewModel>();
            SimpleIoc.Default.Register<ZarejestrujViewModel>();
            SimpleIoc.Default.Register<EdytujViewModel>();
            SimpleIoc.Default.Register<WynikiZaakceptowaneViewModel>();
        }

        /// <summary>
        /// Tworzy servis pozwalaj�cy zmieniac strony z widoku-modelu za pomoc� klucza typu string.
        /// </summary>
        /// <returns>Instancja serwisu.</returns>
        private INavigationService CreateNavigationService()
        {
            var navigationService = new NavigationService();

            navigationService.Configure("CzatPage", typeof(CzatPage));
            navigationService.Configure("EdytujPage", typeof(EdytujPage));
            navigationService.Configure("LogowaniePage", typeof(LogowaniePage));
            navigationService.Configure("MainMenu", typeof(MainMenu));
            navigationService.Configure("ProfilPage", typeof(ProfilPage));
            navigationService.Configure("WynikiPage", typeof(WynikiPage));
            navigationService.Configure("WyszukiwaniePage", typeof(WyszukiwaniePage));
            navigationService.Configure("ZarejestrujPage", typeof(ZarejestrujPage)); 
            navigationService.Configure("WynikiZaakceptowanePage", typeof(WynikiZaakceptowanePage));

            return navigationService;
        }

        public static void Cleanup()
        {
            // TODO Clear the ViewModels
        }
    }
}