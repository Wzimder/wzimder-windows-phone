﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.UI.Popups;
using Wzimder.API.Adapter;
using Wzimder.API.Adapter.Models;

namespace Wzimder.Shared.ViewModel
{
    public class LogowanieViewModel : ViewModelBase
    {
        //Fields
        private ViewModelLocator serviceLocator;
        private ServerAdapter serverAdapter;
        private INavigationService navigationService;
        private Wzimder.Model.Types.User user;
        private string userLogin = "adolf123@wp.pl";
        private string userPassword = "Adolf123!";
        private LoginResponse lastLoginrResponse;

        //Properties
        public ViewModelLocator ViewModelLocator { get { return ViewModelLocator; } }
        public ServerAdapter ServerAdapter { get { return serverAdapter; } }
        public Wzimder.Model.Types.User User { get { return user; } set { user = value; } }
        public INavigationService NavigationService { get { return navigationService; } }
        public string UserLogin { get { return userLogin; }
            set
            {
                userLogin = value;
                LoginCommand.RaiseCanExecuteChanged();
                RaisePropertyChanged("UserLogin");
            }
        }
        public string UserPassword
        {
            get { return userPassword; }
            set
            {
                userPassword = value;
                LoginCommand.RaiseCanExecuteChanged();
                RaisePropertyChanged("UserPassword");
            }
        }
        public LoginResponse LastLoginResponse { get { return this.lastLoginrResponse; } private set { this.lastLoginrResponse = value; } }

        //Constructors
        public LogowanieViewModel(INavigationService navigationService)
        {
            this.navigationService = navigationService;
            this.serviceLocator = App.Current.Resources["Locator"] as ViewModelLocator;
            this.serverAdapter = this.serviceLocator.ServerAdapter;
            user = new Model.Types.User();
            this.LoginCommand = new RelayCommand(login, canExecuteLoginCommand);
            this.RegisterCommand = new RelayCommand(register);
        }

        //Methods
        private async void login()
        {
            if (!NetworkInterface.GetIsNetworkAvailable())
            {
                MessageDialog msgbox = new MessageDialog("Brak połączenia z internetem.");
                await msgbox.ShowAsync();
                return;
            }

            this.lastLoginrResponse = serverAdapter.AccountApiService.Login(grandType: "password", username: UserLogin, password: UserPassword);
            if (this.lastLoginrResponse.access_token != null)
            {
                //Jeśli zalogowano
                NavigationService.NavigateTo("WyszukiwaniePage");
            }
            else
            {
                //Jesli logowanie nie powiodło się
                NavigationService.NavigateTo("ZarejestrujPage");
            }
        }

        public void Wyloguj()
        {
            this.LastLoginResponse = null;
        }

        private async void register()
        {
            if (!NetworkInterface.GetIsNetworkAvailable())
            {
                MessageDialog msgbox = new MessageDialog("Brak połączenia z internetem.");
                await msgbox.ShowAsync();
                return;
            }

            NavigationService.NavigateTo("ZarejestrujPage");
        }

        public RelayCommand LoginCommand { get; internal set; }
        public RelayCommand RegisterCommand { get; internal set; }

        private bool canExecuteLoginCommand()
        {
            return !string.IsNullOrEmpty(UserLogin) && !string.IsNullOrEmpty(UserPassword);
        }
    }
}
