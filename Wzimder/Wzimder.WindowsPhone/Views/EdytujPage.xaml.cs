﻿using GalaSoft.MvvmLight.Views;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using System.Xml.Linq;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.ApplicationModel.Core;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Imaging;
using Windows.Phone.UI.Input;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using Wzimder.ViewModel;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Wzimder
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class EdytujPage : Page
    {
        EdytujViewModel viewModel;
        CoreApplicationView view;
        String ImagePath;

        public EdytujPage()
        {
            this.InitializeComponent();
            this.viewModel = this.DataContext as EdytujViewModel;
            this.view = CoreApplication.GetCurrentView();
            HardwareButtons.BackPressed += HardwareButtons_BackPressed;
        }

        private void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            Frame frame = Window.Current.Content as Frame;
            if (frame.CanGoBack)
            {
                INavigationService navigation = ServiceLocator.Current.GetInstance<INavigationService>();
                navigation.GoBack();
                e.Handled = true;
            }
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.viewModel.DownloadImage();
            //this.ProgresRingLoadImage.Visibility = Visibility.Collapsed;
        }

        private void WybierzZdjecie_Click(object sender, RoutedEventArgs e)
        {
            ImagePath = string.Empty;
            FileOpenPicker filePicker = new FileOpenPicker();
            filePicker.SuggestedStartLocation = PickerLocationId.PicturesLibrary;
            filePicker.ViewMode = PickerViewMode.Thumbnail;

            // Filter to include a sample subset of file types
            filePicker.FileTypeFilter.Clear();
            filePicker.FileTypeFilter.Add(".bmp");
            filePicker.FileTypeFilter.Add(".png");
            filePicker.FileTypeFilter.Add(".jpeg");
            filePicker.FileTypeFilter.Add(".jpg");

            filePicker.PickSingleFileAndContinue();
            view.Activated += viewActivated;
        }

        private async void viewActivated(CoreApplicationView sender, IActivatedEventArgs args1)
        {
            FileOpenPickerContinuationEventArgs args = args1 as FileOpenPickerContinuationEventArgs;

            if (args != null)
            {
                if (args.Files.Count == 0) return;

                view.Activated -= viewActivated;
                StorageFile storageFile = args.Files[0];
                IRandomAccessStream stream = await storageFile.OpenAsync(Windows.Storage.FileAccessMode.Read);
                //var bitmapImage = new Windows.UI.Xaml.Media.Imaging.BitmapImage();
                //await bitmapImage.SetSourceAsync(stream);

                //var decoder = await Windows.Graphics.Imaging.BitmapDecoder.CreateAsync(stream);
                //ProfilObrazek.Source = bitmapImage;

                this.viewModel.ServerAdapter.PhotosApiService.SendPhotos(
                    token: this.viewModel.ViewModelLocator.LogowanieViewModel.LastLoginResponse,
                    stream: stream.AsStream());

                this.viewModel.DownloadImage();
            }
        }



        //public async Task<WriteableBitmap> GetWritableBitmap(StorageFile storageFile)
        //{
        //    var stream = await storageFile.OpenReadAsync();
        //    var wb = new WriteableBitmap(1, 1);
        //    wb.SetSource(stream);
        //    return wb;
        //}

        //private byte[] ImageToByeArray(WriteableBitmap wbm)
        //{
        //    using (Stream stream = wbm.PixelBuffer.AsStream())
        //    using (MemoryStream memoryStream = new MemoryStream())
        //    {
        //        stream.CopyTo(memoryStream);
        //        return memoryStream.ToArray();
        //    }
        //}

    }
}
