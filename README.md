Projekt WZIMDER - Windows Universal App
====================
## Tablica ogłoszeń:
- - -
Proszę o podanie pełnych opisów commitów, czyli co się zmieniło w wersji. Żadnych pustych commitów i wyrazeń typu "kolejny commit", "poprawki", czy "sadgffdgfdgd..." itd.
Trzymajmy kulturę kodu repozytorium :) 
- - -
Dodatkowo proszę sprawdzać czy program się kompiluje przy wrzucaniu commita. Jeśli program się nie będzie kompilował to zablokuje to pracę całego zespołu.
- - -
Przydatne linki
====================
* [Strona Informacyjna](http://wzimder.azurewebsites.net)
* [Grupa - Facebook](https://www.facebook.com/groups/1506552879662142/)
* [Grupy.xlsx](https://onedrive.live.com/view.aspx?resid=18893B3FAC02A106!95285&ithint=file%2cxlsx&app=Excel&authkey=!AIM3dFAAty5Dpvo)
* [Notes](https://onedrive.live.com/view.aspx?resid=7FCFE85ABBB1FA83!2603&ithint=onenote%2c&app=OneNote&authkey=!ALeXo0WKBJ6uaCk)
* [Lista funkcjonalnoci aplikacji](https://attachment.fbsbx.com/file_download.php?id=1052588354774328&eid=ASubbCX29R_N0GjILzyOXspiraUfWwPSCmgrDInzGJCH9bWLQgQYaLTLuxJDPcknl0g&inline=1&ext=1445168360&hash=ASu4qfFg1R_isYZ5)
* [Repozytorium Backend](https://bitbucket.org/Wzimder/wzimder-backend)
* [Repozytorium Web](https://bitbucket.org/Wzimder/wzimder-web)
* [Repozytorium Android](https://bitbucket.org/Wzimder/wzimder-android)
* [Logo dokumentu WZIMDER](https://www.dropbox.com/sh/v0q8tittwnl9k1d/AACdokborU0s2say5gcLHcQ6a?dl=0)
- - -

[lexers]: http://pygments.org/docs/lexers/
[fireball]: http://daringfireball.net/projects/markdown/ 
[Pygments]: http://www.pygments.org/ 
[Extra]: http://michelf.ca/projects/php-markdown/extra/
[id]: http://example.com/  "Optional Title Here"
[BBmarkup]: https://confluence.atlassian.com/x/xTAvEw