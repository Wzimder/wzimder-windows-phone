﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wzimder.Model.Types;
using Wzimder.API.Adapter.Models;

namespace Wzimder.API.Adapter.Interfaces
{
    public interface IMessages
    {
        /// <summary>
        /// Pobranie nieprzeczytanych wiadomości.
        /// Metoda GET.
        /// http://52.30.121.166/Backend/Help/Api/GET-api-messages
        /// </summary>
        /// <returns>Lista nieprzeczytanych wiadomości.</returns>
        List<Message> GetMessages(LoginResponse token);
        /// <summary>
        /// Pobranie nieprzeczytanych wiadomości od konkretnego użytkownika.
        /// Metoda GET.
        /// http://52.30.121.166/Backend/Help/Api/GET-api-messages-id
        /// </summary>
        /// <param name="id">Id usera do którego należą wiadomości.</param>
        /// <returns></returns>
        List<Message> GetMessages(LoginResponse token, int id);
        /// <summary>
        /// Wysłanie wiadomości do innego użytkownika.
        /// Metoda POST.
        /// http://52.30.121.166/Backend/Help/Api/POST-api-messages
        /// </summary>
        /// <param name="text">Treść wiadomości.</param>
        /// <param name="reciepientId">Id usera - cel wiadomości.</param>
        /// <returns>Czy operacja zakończyła się powodzeniem.</returns>
        bool SendMessages(LoginResponse token, string text, string reciepientId);
    }
}
