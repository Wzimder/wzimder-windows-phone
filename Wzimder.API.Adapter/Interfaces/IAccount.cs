﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wzimder.API.Adapter.Models;
using Wzimder.Model.Types;

namespace Wzimder.API.Adapter.Interfaces
{
    public interface IAccount
    {
        /// <summary>
        /// Wylogowanie użytkownika.
        /// Metoda POST.
        /// http://52.30.121.166/Backend/Help/Api/POST-api-account-login
        /// </summary>
        /// <param name="grandType">Rodzaj praw.</param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns>Czy operacja zakończyła sie powodzeniem.</returns>
        LoginResponse Login(string grandType, string username, string password);
        /// <summary>
        /// opcjonalne, aby się wylogować należy "zapomnieć" token.
        /// http://52.30.121.166/Backend/Help/Api/POST-api-account-register
        /// </summary>
        void Logout(LoginResponse token);
        /// <summary>
        /// ustawienie lokacji
        /// Metoda POST.
        /// http://52.30.121.166/Backend/Help/Api/POST-api-account-location
        /// </summary>
        /// <returns>Czy operacja zakończyła sie powodzeniem.</returns>
        bool Location(LoginResponse token, Geolocation location);
        /// <summary>
        /// Rejestracja użytkownika.
        /// Metoda POST.
        /// http://backendaw.azurewebsites.net/Help/Api/POST-api-Account-RegisterExternal
        /// </summary>
        /// <param name="email"></param>
        /// <returns>Czy operacja zakończyła się powodzeniem.</returns>
        bool Register(RegisterRequest registerRequest);
        /// <summary>
        /// Zmiana hasła.
        /// http://52.30.121.166/Backend/Help/Api/POST-api-account-changepassword
        /// </summary>
        /// <param name="changePasswordRequest"></param>
        /// <returns></returns>
        bool ChangePassword(LoginResponse token, ChangePasswordRequest changePasswordRequest);

        //Metoda zwracająca dane o użytkowniku
        User GetUser(LoginResponse token);

        //Metoda zmieniająca zdjęcie profilowe użytkownika
        void SetProfilePhoto(LoginResponse token, int id);

        //Metoda zmieniająca opis użytkownika
        void SetDescription(LoginResponse token, string description);
    }

    
}
