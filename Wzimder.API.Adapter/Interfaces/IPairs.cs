﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wzimder.Model.Types;
using Wzimder.API.Adapter.Models;

namespace Wzimder.API.Adapter.Interfaces
{
    public interface IPairs
    {
        /// <summary>
        /// Pobranie par użytkownika.
        /// Metoda GET.
        /// http://52.30.121.166/Backend/Help/Api/GET-api-pairs
        /// </summary>
        /// <returns>Lista użytkowników.</returns>
        List<User> GetPairs(LoginResponse token);
        /// <summary>
        /// Polubienie/Odrzucenie innego użytkownika.
        /// Metoda POST.
        /// http://52.30.121.166/Backend/Help/Api/POST-api-pairs
        /// </summary>
        /// <param name="newPair">Nowa para.</param>
        /// <returns>Czy operacja zakończyła się powodzeniem.</returns>
        bool SendPairs(LoginResponse token, Pair newPair);
    }
}
