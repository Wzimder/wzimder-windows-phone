﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wzimder.Model.Types;
using Wzimder.API.Adapter.Models;

namespace Wzimder.API.Adapter.Interfaces
{
    public interface ISearchOptions
    {
        /// <summary>
        /// Pobranie ustawień wyszukiwania.
        /// Metoda GET.
        /// http://52.30.121.166/Backend/Help/Api/GET-api-searchoptions
        /// </summary>
        /// <returns>Ustawienia wyszukiwania.</returns>
        SearchOption GetSearchOptions(LoginResponse token);
        /// <summary>
        /// Ustawienie ustawień wyszukiwania.
        /// Metoda PUT.
        /// http://52.30.121.166/Backend/Help/Api/PUT-api-searchoptions
        /// </summary>
        /// <param name="updatedSearchOptions">Nowe dane ustawień wyszukiwania.</param>
        /// <returns>Czy operacja zakończyła się powodzeniem.</returns>
        bool SendSearchOptions(LoginResponse token, SearchOption updatedSearchOptions);
    }
}
