﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wzimder.Model.Types;
using Wzimder.API.Adapter.Models;

namespace Wzimder.API.Adapter.Interfaces
{
    public interface IPhotos
    {
        /// <summary>
        /// Usunięcie zdjęcia.
        /// Metoda DELETE.
        /// http://backendaw.azurewebsites.net/Help/Api/DELETE-api-Photos-id
        /// </summary>
        /// <param name="id">id zdjęcia do usunięcia.</param>
        /// <returns>Czy operacja zakończyła sie powodzeniem.</returns>
        bool DeletePhotos(LoginResponse token, int id);
        /// <summary>
        /// Pobranie linkow do zdjeć.
        /// Metoda GET.
        /// http://backendaw.azurewebsites.net/Help/Api/GET-api-Photos
        /// </summary>
        /// <returns>Lista wszytskich linków do zdjęć.</returns>
        int[] GetPhotos(LoginResponse token);
        /// <summary>
        /// pobranie jednego zdjecia.
        /// Metoda GET.
        /// http://backendaw.azurewebsites.net/Help/Api/GET-api-Photos-id
        /// </summary>
        /// <param name="id">id zdjęcia.</param>
        /// <returns>Czy operacja się udała.</returns>
        byte[] GetPhotos(LoginResponse token, int id);
        /// <summary>
        /// Dodanie zdjecia.
        /// Metoda POST.
        /// http://backendaw.azurewebsites.net/Help/Api/POST-api-Photos
        /// </summary>
        /// <param name="photoToSend">Url do zdjęcia do wysłania.</param>
        /// <returns>Czy operacja się udała.</returns>
        bool SendPhotos(LoginResponse token, byte[] obrazek);
        /// <summary>
        /// Dodanie zdjecia.
        /// Metoda POST.
        /// http://backendaw.azurewebsites.net/Help/Api/POST-api-Photos
        /// </summary>
        /// <param name="token">Do identyfikacji usera.</param>
        /// <param name="stream">Strumien zdjęcia.</param>
        /// <returns></returns>
        bool SendPhotos(LoginResponse token, System.IO.Stream stream);
    }
}
