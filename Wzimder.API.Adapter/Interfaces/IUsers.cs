﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wzimder.Model.Types;
using Wzimder.API.Adapter.Models;

namespace Wzimder.API.Adapter.Interfaces
{
    public interface IUsers
    {
        /// <summary>
        /// Ustawienie aktualnej lokalizacji.
        /// </summary>
        /// <param name="geolocation">Obecna lokalizacja usera.</param>
        /// <returns>Czy operacja zakończyła się powodzeniem.</returns>
        //bool SetLocation(Geolocation geolocation);
        /// <summary>
        /// Pobranie osób pasujących do ustawionych ustawień wyszukiwania.
        /// </summary>
        /// <returns>Lista obiektów typu user, zgodna z kryteriami wyszukiwania.</returns>
        List<User> GetUsers(LoginResponse token);
        /// <summary>
        /// Pobranie dokładnych informacji o innym użytkowniku.
        /// </summary>
        /// <param name="id">Id usera, dla którego chcemy pobrać dane.</param>
        /// <returns>Pobrany dokłady user.</returns>
        User GetUsers(LoginResponse token, string id);
        /// <summary>
        /// Zmiana danych personalnych.
        /// </summary>
        /// <param name="updatedUser">Obiekt User z zaktualizowanymi danymi.</param>
        /// <returns>Czy operacja zakończyła się powodzeniem.</returns>
        //bool SendUsers(User updatedUser);
    }
}
