﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Wzimder.API.Adapter.Interfaces;
using Wzimder.API.Adapter.Methods;

namespace Wzimder.API.Adapter
{
    /// <summary>
    /// Obiekt odpowiedzialny za udostępnienie połączenia z API serwera.
    /// </summary>
    public class ServerAdapter
    {
        /// <summary>
        /// Przełącznik do danych testowych.
        /// True - z biblioteki zwacane są dane testowe.
        /// False - dane są pobierane z serwera.
        /// </summary>
        public static bool SampleData = false;

        /*
        *   Każdy obiekt - serwis jest odpowiedzialny za część metod które,
        *   odczytują, zapisują, usuwają lub zmieniają stan danych na serwrerze.
        *
        *   Obiekty są stworzone tylko raz w programie - ponownemu stworzeniu zapobiegają 
        *   specyfikatory dostępu dla klas, które są internal dla projektu.
        *
        *   Nie zmieniać nic w architekturze bez omówienia z architektem.
        */

        //Fields
        private IAccount account;
        private IMessages messages;
        private IPairs pairs;
        private IPhotos photos;
        private ISearchOptions searchOprions;
        private IUsers users;

        //Properties
        public IAccount AccountApiService { get { return this.account; } }
        public IMessages MessagesApiService { get { return this.messages; } }
        public IPairs PairsApiService { get { return this.pairs; } }
        public IPhotos PhotosApiService { get { return this.photos; } }
        public ISearchOptions SearchOptionsApiService { get { return this.searchOprions; } }
        public IUsers UsersApiService { get { return this.users; } }

        //Constructor
        public ServerAdapter()
        {
            this.Initialize();
        }

        //Methods
        private void Initialize()
        {
            this.account = new Account();
            this.messages = new Messages();
            this.pairs = new Pairs();
            this.photos = new Photos();
            this.searchOprions = new SearchOptions();
            this.users = new Users();
        }
    }
}
