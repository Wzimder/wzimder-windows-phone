﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wzimder.Model.Types;

namespace Wzimder.API.Adapter.Models
{
    /// <summary>
    /// Dane używane podczas restracji użytkownika.
    /// </summary>
    public class RegisterRequest
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Gender Gender { get; set; }
        public DateTime BirthDate { get; set; }
        public string Description { get; set; }
    }
}
