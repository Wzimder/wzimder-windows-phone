﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wzimder.API.Adapter.Models
{
    /// <summary>
    /// Obiekt - Response dla metody Login.
    /// Więcej informacji na:
    /// http://52.30.121.166/Backend/Help/Api/POST-api-account-login
    /// </summary>
    public class LoginResponse 
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
        public int expires_in { get; set; }
        public string userName { get; set; }
        public string issued { get; set; }
        public string expires { get; set; }
    }
}
