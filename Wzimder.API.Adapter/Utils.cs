﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Wzimder.API.Adapter
{
    class Utils
    {
        public bool WalidujEmail(string email)
        {
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = regex.Match(email);
            if (!match.Success)
                return false;
            else
            {
                return true;
            }
        }

        public bool SprawdzMinDlugosc(string password, int min)
        {
            if (password.Length < min)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool SprawdzMaxDlugosc(string password, int max)
        {
            if (password.Length > max)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool SprawdzMaleLitery(string password)
        {
            bool hasSmallLetter = false;
            foreach (char c in password)
            {
                if (char.IsLetter(c) && char.IsLower(c))
                {
                    hasSmallLetter = true;
                    break;
                }
            }
            return hasSmallLetter;
        }

        public bool SprawdzDuzeLitery(string password)
        {
            bool hasBigLetter = false;
            foreach (char c in password)
            {
                if (char.IsLetter(c) && char.IsUpper(c))
                {
                    hasBigLetter = true;
                    break;
                }
            }
            return hasBigLetter;
        }

        public bool SprawdzDziwneZnaki(string password)
        {
            string requiredChars = "!@#$%^&*._";
            bool hasRequiredChar = false;
            foreach (char c in password)
            {
                foreach (char cc in requiredChars)
                {
                    if (c == cc)
                    {
                        hasRequiredChar = true;
                        break;
                    }
                }
                if (hasRequiredChar) break;
            }
            return hasRequiredChar;
        }

        public bool SprawdzNiedopuszczalneZnaki(string password)
        {
            string forbiddenChars = "()";
            bool hasForbiddenChar = false;
            foreach (char c in password)
            {
                foreach (char cc in forbiddenChars)
                {
                    if (c == cc)
                    {
                        hasForbiddenChar = true;
                        break;
                    }
                }
                if (hasForbiddenChar) break;
            }
            return hasForbiddenChar;
        }

        public bool WalidujHaslo(string password)
        {
            bool wynik = false;
            if (SprawdzMinDlugosc(password,6))
            {
                if (SprawdzMaxDlugosc(password,100))
                {
                    if (SprawdzMaleLitery(password))
                    {
                        if (SprawdzDuzeLitery(password))
                        {
                            if (SprawdzDziwneZnaki(password))
                            {
                                if (!SprawdzNiedopuszczalneZnaki(password))
                                {
                                    wynik = true;
                                }
                            }
                        }
                    }
                }
            }
            return wynik;
        }
    }
}
