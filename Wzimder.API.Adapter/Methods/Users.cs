﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wzimder.API.Adapter.Interfaces;
using Wzimder.API.Adapter.Models;
using Wzimder.Model.Types;
using Windows.Web.Http;
using Windows.Foundation;

namespace Wzimder.API.Adapter.Methods
{
    internal class Users : IUsers
    {
        /// <summary>
        /// Ustawienie aktualnej lokalizacji.
        /// </summary>
        /// <param name="geolocation">Obecna lokalizacja usera.</param>
        /// <returns>Czy operacja zakończyła się powodzeniem.</returns>
        //public bool SetLocation(Geolocation geolocation)
        //{
        //    //Nie komentować!!!

        //    if (ServerAdapter.SampleData)
        //    {
        //        //Tutaj zwróć przykładowe dane takie jak w dokumentacji
        //        return true;
        //    }
        //    else
        //    {
        //        //Tutaj zwróć dane z serwra
        //        return false;
        //    }

        //    //throw new NotImplementedException();
        //}

        /// <summary>
        /// Pobranie osób pasujących do ustawionych ustawień wyszukiwania.
        /// http://52.30.121.166/Backend/Help/Api/GET-api-users
        /// </summary>
        /// <returns>Lista obiektów typu user, zgodna z kryteriami wyszukiwania.</returns>
        public List<User> GetUsers(LoginResponse token)
        {
            List<User> listaUser;
            if (ServerAdapter.SampleData)
            {
                //Tutaj zwróć przykładowe dane takie jak w dokumentacji
                listaUser = new List<User> { };
                User user = new User();
                user.Id = "2";
                user.FirstName = "Kasia";
                user.BirthDate = new DateTime(1995, 5, 12);
                user.ProfilePhotoId = 0;
                listaUser.Add(user);
            }
            else
            {
                Uri uri = new Uri("http://52.30.121.166/Backend/api/users");
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Authorization = new Windows.Web.Http.Headers.HttpCredentialsHeaderValue("Bearer", token.access_token);
                IAsyncOperationWithProgress<string, HttpProgress> responseTask = client.GetStringAsync(uri);
                while (responseTask.Status != AsyncStatus.Completed && responseTask.Status != AsyncStatus.Error) { /*Czekanie na skończenie Asynchronicznej metody.*/ }
                string response = responseTask.GetResults();
                listaUser = Newtonsoft.Json.JsonConvert.DeserializeObject<List<User>>(response);
                return listaUser;
            }

            return listaUser;
        }

        /// <summary>
        /// Pobranie dokładnych informacji o innym użytkowniku.
        /// http://52.30.121.166/Backend/Help/Api/GET-api-users-id
        /// </summary>
        /// <param name="id">Id usera, dla którego chcemy pobrać dane.</param>
        /// <returns>Pobrany dokłady user.</returns>
        public User GetUsers(LoginResponse token, string id)
        {
            if (ServerAdapter.SampleData)
            {
                //Tutaj zwróć przykładowe dane takie jak w dokumentacji
                User user = new User();
                user.Id = id;
                user.FirstName = "Kasia";
                user.BirthDate = new DateTime(1995, 5, 12);
                user.LastName = "Kowalska";
                user.Gender = Gender.Female;
                user.Description = "Ładna";
                Photo p = new Photo();
                Photo p2 = new Photo();
                user.Photos.Add(p);
                user.Photos.Add(p);
                user.ProfilePhotoId = 0;

                return user;
            }
            else
            {
                if (id == null)
                    throw new Exception("Id - identyfikator użtykownika nie moze być nullem.");

                //string link = "http://52.30.121.166/Backend/api/users/" + id;
                Uri uri = new Uri("http://52.30.121.166/Backend/api/users/" + id);
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Authorization = new Windows.Web.Http.Headers.HttpCredentialsHeaderValue("Bearer", token.access_token);
                IAsyncOperationWithProgress<string, HttpProgress> responseTask = client.GetStringAsync(uri);
                while (responseTask.Status != AsyncStatus.Completed && responseTask.Status != AsyncStatus.Error) { /*Czekanie na skończenie Asynchronicznej metody.*/ }
                string response = responseTask.GetResults();
                User user = Newtonsoft.Json.JsonConvert.DeserializeObject<User>(response);
                return user;
            }
        }

        /// <summary>
        /// Zmiana danych personalnych.
        /// </summary>
        /// <param name="updatedUser">Obiekt User z zaktualizowanymi danymi.</param>
        /// <returns>Czy operacja zakończyła się powodzeniem.</returns>
        //public bool SendUsers(User updatedUser)
        //{
        //    //Nie komentować!!!

        //    if (ServerAdapter.SampleData)
        //    {
        //        //Tutaj zwróć przykładowe dane takie jak w dokumentacji
        //        return true;
        //    }
        //    else
        //    {
        //        //Tutaj zwróć dane z serwra
        //        return false;
        //    }

        //    //throw new NotImplementedException();
        //}
    }
}
