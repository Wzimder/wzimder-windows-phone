﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wzimder.API.Adapter.Interfaces;
using Wzimder.Model.Types;
using Windows.Web.Http;
using Wzimder.API.Adapter.Models;
using Windows.Foundation;

namespace Wzimder.API.Adapter.Methods
{
    internal class Messages : IMessages
    {
        /// <summary>
        /// Pobranie nieprzeczytanych wiadomości.
        /// Metoda GET.
        /// http://backendaw.azurewebsites.net/Help/Api/GET-api-Messages
        /// </summary>
        /// <returns>Lista nieprzeczytanych wiadomości.</returns>
        public List<Message> GetMessages(LoginResponse token)
        {
            List<Message> listaWiadomosci;
            if (ServerAdapter.SampleData)
            {
                //Tutaj zwróć przykładowe dane takie jak w dokumentacji
                listaWiadomosci = new List<Message> { };
                Message m1 = new Message();
                User sender = new User();
                sender.Id = "2";
                m1.Sender = sender;
                m1.Text = "Hi";
                listaWiadomosci.Add(m1);
                Message m2 = new Message();
                User sender2 = new User();
                m2.Sender = sender2;
                m2.Text = "Hello";
                listaWiadomosci.Add(m2);
            }
            else
            {
                //Tutaj zwróć dane z serwra
                string link = "http://52.30.121.166/Backend/api/messages";
                HttpClient client = new HttpClient();
                //client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token.access_token);
                client.DefaultRequestHeaders.Authorization = new Windows.Web.Http.Headers.HttpCredentialsHeaderValue("Bearer", token.access_token);
                Uri uri = new Uri(link);
                var response = client.GetStringAsync(uri);
                while (response.Status != AsyncStatus.Completed)
                {
                    //Czekanie na skończenie Asynchronicznej metody.
                }
                listaWiadomosci = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Message>>(response.GetResults());
            }

            //throw new NotImplementedException();
            return listaWiadomosci;
        }

        /// <summary>
        /// Pobranie nieprzeczytanych wiadomości od konkretnego użytkownika.
        /// Metoda GET.
        /// http://backendaw.azurewebsites.net/Help/Api/GET-api-Messages-id
        /// </summary>
        /// <param name="id">Id usera do którego należą wiadomości.</param>
        /// <returns></returns>
        public List<Message> GetMessages(LoginResponse token, int id)
        {
            List<Message> listaWiadomosci;
            if (ServerAdapter.SampleData)
            {
                //Tutaj zwróć przykładowe dane takie jak w dokumentacji
                listaWiadomosci = new List<Message> { };
                Message m1 = new Message();
                User sender = new User();
                sender.Id = "2";
                m1.Sender = sender;
                m1.Text = "Hi";
                listaWiadomosci.Add(m1);
                Message m2 = new Message();
                User sender2 = new User();
                m2.Sender = sender2;
                m2.Text = "Hello";
                listaWiadomosci.Add(m2);
            }
            else
            {
                //Tutaj zwróć dane z serwra
                string link = "http://52.30.121.166/Backend/api/messages/{" + id + "}";
                HttpClient client = new HttpClient();
                //client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token.access_token);
                client.DefaultRequestHeaders.Authorization = new Windows.Web.Http.Headers.HttpCredentialsHeaderValue("Bearer", token.access_token);
                Uri uri = new Uri(link);
                var response = client.GetStringAsync(uri);
                while (response.Status != AsyncStatus.Completed)
                {
                    //Czekanie na skończenie Asynchronicznej metody.
                }
                listaWiadomosci = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Message>>(response.GetResults());
            }

            //throw new NotImplementedException();
            return listaWiadomosci;
        }

        /// <summary>
        /// Wysłanie wiadomości do innego użytkownika.
        /// Metoda POST.
        /// http://backendaw.azurewebsites.net/Help/Api/POST-api-Messages
        /// </summary>
        /// <param name="text">Treść wiadomości.</param>
        /// <param name="reciepientId">Id usera - cel wiadomości.</param>
        /// <returns>Czy operacja zakończyła się powodzeniem.</returns>
        public bool SendMessages(LoginResponse token, string text, string reciepientId)
        {
            if (ServerAdapter.SampleData)
            {
                return true;
            }
            else
            {
                Uri uri = new Uri("http://52.30.121.166/Backend/api/messages");
                string json = "{" + string.Format("\"Text\":\"{0}\",\"RecipientId\":\"{1}\"", text, reciepientId) + "}";
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Authorization = new Windows.Web.Http.Headers.HttpCredentialsHeaderValue("Bearer", token.access_token);
                HttpStringContent content = new HttpStringContent(json,Windows.Storage.Streams.UnicodeEncoding.Utf8, "application/json");
                IAsyncOperationWithProgress<HttpResponseMessage, HttpProgress> responseTask = client.PostAsync(uri, content);
                while (!(responseTask.Status == AsyncStatus.Completed)) { /* Czekanie na skończenie Asynchronicznej metody. */ }
                HttpResponseMessage response = responseTask.GetResults();
                return response.IsSuccessStatusCode;
            }
        }
    }
}
