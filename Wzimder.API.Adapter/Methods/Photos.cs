﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wzimder.API.Adapter.Interfaces;
using Wzimder.Model.Types;
using Wzimder.API.Adapter.Models;
using Windows.Web.Http;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Wzimder.API.Adapter.Methods
{
    internal class Photos : IPhotos
    {
        /// <summary>
        /// Usunięcie zdjęcia.
        /// Metoda DELETE.
        /// http://backendaw.azurewebsites.net/Help/Api/DELETE-api-Photos-id
        /// </summary>
        /// <param name="id">id zdjęcia do usunięcia.</param>
        /// <returns>Czy operacja zakończyła sie powodzeniem.</returns>
        public bool DeletePhotos(LoginResponse token, int id)
        {
            if (ServerAdapter.SampleData)
            {
                //Tutaj zwróć przykładowe dane takie jak w dokumentacji
                return true;
            }
            else
            {
                //Tutaj zwróć dane z serwra
                string link = "http://52.30.121.166/Backend/api/Photos/" + id;
                Windows.Web.Http.HttpClient client = new Windows.Web.Http.HttpClient();
                client.DefaultRequestHeaders.Authorization = new Windows.Web.Http.Headers.HttpCredentialsHeaderValue("Bearer", token.access_token);
                Uri uri = new Uri(link);
                var response = client.DeleteAsync(uri).GetResults();
                //while (response.Status != AsyncStatus.Completed)
                //{
                //    //Czekanie na skończenie Asynchronicznej metody.
                //}
                return true;
            }

            //throw new NotImplementedException();
        }

        /// <summary>
        /// Pobranie linkow do zdjeć.
        /// Metoda GET.
        /// http://backendaw.azurewebsites.net/Help/Api/GET-api-Photos
        /// </summary>
        /// <returns>Lista wszytskich linków do zdjęć.</returns>
        /// JESZCZE NIE DOKONCZONE!!!
        public int[] GetPhotos(LoginResponse token)
        {
            int[] lista;
            if (ServerAdapter.SampleData)
            {
                //Tutaj zwróć przykładowe dane takie jak w dokumentacji
                //listaZdjec = new List<Photo> { };
                //Photo foto = new Photo();
                //foto.Id = 1;
                //foto.UserId = 1;
                //listaZdjec.Add(foto);
                lista = new int[] { };
            }
            else
            {
                string link = "http://52.30.121.166/Backend/api/Photos";
                System.Net.Http.HttpClient client = new System.Net.Http.HttpClient();
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token.access_token);
                Uri uri = new Uri(link);
                Task<Stream> streamTask = client.GetStreamAsync(uri);
                while (!streamTask.IsCompleted) { }
                StreamReader sr = new StreamReader(streamTask.Result);
                
                string json = sr.ReadToEnd();
                //Tu należy skonwetować json na lsię intów!!!
                lista = new int[] { };
            }

            return lista;
        }

        /// <summary>
        /// pobranie jednego zdjecia.
        /// Metoda GET.
        /// http://backendaw.azurewebsites.net/Help/Api/GET-api-Photos-id
        /// </summary>
        /// <param name="id">id zdjęcia.</param>
        /// <returns>Czy operacja się udała.</returns>
        public byte[] GetPhotos(LoginResponse token, int id)
        {
            if (ServerAdapter.SampleData)
            {
                //Tutaj zwróć przykładowe dane takie jak w dokumentacji
                return new byte[] { };
            }
            else
            {
                //Tutaj zwróć dane z serwra
                string link = "http://52.30.121.166/Backend/api/Photos/" + id;
                System.Net.Http.HttpClient client = new System.Net.Http.HttpClient();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.access_token);
                System.Net.Http.HttpResponseMessage response = client.GetAsync(link).Result;
                Stream stream = response.Content.ReadAsStreamAsync().Result;
                byte[] bytesInStream = new byte[stream.Length];
                stream.Read(bytesInStream, 0, (int)bytesInStream.Length);
                return bytesInStream;
            }

            //throw new NotImplementedException();
        }

        /// <summary>
        /// Dodanie zdjecia.
        /// Metoda POST.
        /// http://backendaw.azurewebsites.net/Help/Api/POST-api-Photos
        /// </summary>
        /// <param name="photoToSend">Url do zdjęcia do wysłania.</param>
        /// <returns>Czy operacja się udała.</returns>
        /// NIE WIADOMO CZY DZIAŁA!!!
        public bool SendPhotos(LoginResponse token, byte[] obrazek)
        {
            if (ServerAdapter.SampleData)
            {
                //Tutaj zwróć przykładowe dane takie jak w dokumentacji
                return true;
            }
            else
            {
                //Uri uri = new Uri("http://52.30.121.166/Backend/api/Photos");
                string link = "http://52.30.121.166/Backend/api/Photos";
                System.Net.Http.HttpClient client = new System.Net.Http.HttpClient();
                //client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token.access_token);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.access_token);
                MultipartFormDataContent form = new MultipartFormDataContent();
                ByteArrayContent content = new ByteArrayContent(obrazek);
                form.Add(content);
                Task<System.Net.Http.HttpResponseMessage> responseTask = client.PostAsync(link, form);
                while (!responseTask.IsCompleted) { /* Czekanie na skończenie Asynchronicznej metody. */ }
                System.Net.Http.HttpResponseMessage response = responseTask.Result;
                //var response = client.PostAsync(link, form).Result;
                return response.IsSuccessStatusCode;
            }
        }

        /// <summary>
        /// METODA WYSYŁAJĄCA ZDJĘCIE ZA POMOCĄ STREAM!!!
        /// </summary>
        /// <param name="token"></param>
        /// <param name="stream"></param>
        /// <returns></returns>
        public bool SendPhotos(LoginResponse token, Stream stream)
        {
            if (ServerAdapter.SampleData)
            {
                //Tutaj zwróć przykładowe dane takie jak w dokumentacji
                return true;
            }
            else
            {
                //Uri uri = new Uri("http://52.30.121.166/Backend/api/Photos");
                string link = "http://52.30.121.166/Backend/api/Photos";
                System.Net.Http.HttpClient client = new System.Net.Http.HttpClient();
                //client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token.access_token);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.access_token);
                MultipartFormDataContent form = new MultipartFormDataContent();
                HttpContent content = new StreamContent(stream);
                form.Add(content);
                Task<System.Net.Http.HttpResponseMessage> responseTask = client.PostAsync(link, form);
                while (!responseTask.IsCompleted) { /* Czekanie na skończenie Asynchronicznej metody. */ }
                System.Net.Http.HttpResponseMessage response = responseTask.Result;
                //var response = client.PostAsync(link, form).Result;
                return response.IsSuccessStatusCode;
            }
        }
    }
}
