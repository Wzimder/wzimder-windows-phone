﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wzimder.API.Adapter.Interfaces;
using Wzimder.Model.Types;
using Wzimder.API.Adapter.Models;
using Windows.Web.Http;
using Windows.Foundation;

namespace Wzimder.API.Adapter.Methods
{
    internal class SearchOptions : ISearchOptions
    {
        /// <summary>
        /// Pobranie ustawień wyszukiwania.
        /// Metoda GET.
        /// http://backendaw.azurewebsites.net/Help/Api/GET-api-SearchOptions
        /// </summary>
        /// <returns>Ustawienia wyszukiwania.</returns>
        public SearchOption GetSearchOptions(LoginResponse token)
        {
            SearchOption opcja;
            if (ServerAdapter.SampleData)
            {
                //Tutaj zwróć przykładowe dane takie jak w dokumentacji
                opcja = new SearchOption();
                opcja.Gender = Gender.Male;
                opcja.MinAge = 18;
                opcja.MaxAge = 23;
                opcja.MaxDistanceKM = 10;
            }
            else
            {
                //Tutaj zwróć dane z serwra
                string link = "http://52.30.121.166/Backend/api/searchoptions";
                //using (var webClient = new WebClient())
                //{
                //    //webClient.Headers.Add(HttpRequestHeader.Accept, "application/json");
                //    webClient.Headers.Add(HttpRequestHeader.Authorization, "Bearer " + token.access_token);
                //    //var response = webClient.UploadString(link, "GET", "");
                //    var response = webClient.DownloadString(link);
                //    opcja = Newtonsoft.Json.JsonConvert.DeserializeObject<List<User>>(response);
                //}
                HttpClient client = new HttpClient();
                //client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token.access_token);
                client.DefaultRequestHeaders.Authorization = new Windows.Web.Http.Headers.HttpCredentialsHeaderValue("Bearer", token.access_token);
                Uri uri = new Uri(link);
                var response = client.GetStringAsync(uri);
                while (response.Status != AsyncStatus.Completed)
                {
                    //Czekanie na skończenie Asynchronicznej metody.
                }
                opcja = Newtonsoft.Json.JsonConvert.DeserializeObject<SearchOption>(response.GetResults());
                return opcja;
            }

            //throw new NotImplementedException();
            return opcja;
        }

        /// <summary>
        /// Ustawienie ustawień wyszukiwania.
        /// Metoda PUT.
        /// http://backendaw.azurewebsites.net/Help/Api/PUT-api-SearchOptions
        /// </summary>
        /// <param name="updatedSearchOptions">Nowe dane ustawień wyszukiwania.</param>
        /// <returns>Czy operacja zakończyła się powodzeniem.</returns>
        public bool SendSearchOptions(LoginResponse token, SearchOption wyszukiwanie)
        {
            if (ServerAdapter.SampleData)
            {
                //Tutaj zwróć przykładowe dane takie jak w dokumentacji
                return true;
            }
            else
            {
                //Tutaj zwróć dane z serwra
                string link = "http://52.30.121.166/Backend/api/searchoptions";
                string json = "{" + String.Format("\"MinAge\":{0},\"MaxAge\":{1},\"Gender\":{2},\"MaxDistanceKM\":{3}", wyszukiwanie.MinAge, wyszukiwanie.MaxAge, (int)wyszukiwanie.Gender, wyszukiwanie.MaxDistanceKM) + "}";
                //using (var client = new WebClient())
                //{
                //    client.Headers.Add(HttpRequestHeader.Authorization, "Bearer " + token.access_token);
                //    client.Headers.Add(HttpRequestHeader.Accept, "application/json");
                //    client.Headers.Add(HttpRequestHeader.ContentType, "application/json");
                //    var response = client.UploadString(link, "PUT", json);
                //}
                HttpClient client = new HttpClient();
                //client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token.access_token);
                client.DefaultRequestHeaders.Authorization = new Windows.Web.Http.Headers.HttpCredentialsHeaderValue("Bearer", token.access_token);
                //client.DefaultRequestHeaders.Add("Accept", "application/json");
                //client.DefaultRequestHeaders.Add("ContentType", "application/json");
                Uri uri = new Uri(link);
                HttpStringContent content = new HttpStringContent(json,Windows.Storage.Streams.UnicodeEncoding.Utf8, "application/json");
                var response = client.PutAsync(uri, content);
                //while (response.Status != AsyncStatus.Completed)
                //{
                //    //Czekanie na skończenie Asynchronicznej metody.
                //}
                return true;
            }

            //throw new NotImplementedException();
        }
    }
}
