﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wzimder.API.Adapter.Interfaces;
using Wzimder.Model.Types;
using Wzimder.API.Adapter.Models;
using Windows.Web.Http;
using System.Net;
using Windows.Foundation;
using Windows.Web.Http.Headers;
using Newtonsoft.Json;
using System.Text.RegularExpressions;

namespace Wzimder.API.Adapter.Methods
{
    internal class Account : IAccount
    {
        /// <summary>
        /// Wylogowanie użytkownika.
        /// Metoda POST.
        /// http://52.30.121.166/Backend/Help/Api/POST-api-account-login
        /// </summary>
        /// <param name="grandType">Rodzaj praw.</param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns>Czy operacja zakończyła sie powodzeniem.</returns>
        public LoginResponse Login(string grandType, string username, string password)
        {
            string tresc = String.Format("grant_type={0}&username={1}&password={2}", grandType, username, password);
            Uri uri = new Uri("http://52.30.121.166/Backend/api/account/login");
            HttpStringContent content = new HttpStringContent(tresc);
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new HttpMediaTypeWithQualityHeaderValue("application/json"));
            IAsyncOperationWithProgress<HttpResponseMessage, HttpProgress> responseTask = httpClient.PostAsync(uri, content);
            while (responseTask.Status != AsyncStatus.Completed) { /*Czekanie na skończenie Asynchronicznej metody. */ }
            IHttpContent resultContent = responseTask.GetResults().Content;
            IAsyncOperationWithProgress<string, ulong> response = resultContent.ReadAsStringAsync();
            while (response.Status != AsyncStatus.Completed) { /*Czekanie na skończenie Asynchronicznej metody. */ }
            string result = response.GetResults();
            LoginResponse APIresponse = JsonConvert.DeserializeObject<LoginResponse>(result);

            return APIresponse;
        }

        #region Old Code
        //var result = LoginAsync(grandType, username, password)?.Result;
        //return result;


        public LoginResponse LoginAsync(string grandType, string username, string password)
        {
            if (ServerAdapter.SampleData)
            {
                //Tutaj zwróć przykładowe dane takie jak w dokumentacji
                return new LoginResponse();
            }
            else
            {
                //Tutaj zwróć dane z serwra
                //WebRequest request = WebRequest.Create("http://52.30.121.166/Backend/api/account/login");
                //request.Method = "POST";

                string tresc = String.Format("grant_type={0}&username={1}&password={2}", grandType, username, password);
                //byte[] byteArray = Encoding.UTF8.GetBytes(tresc);
                //request.ContentType = "application/x-www-form-urlencoded";
                //request.ContentLength = byteArray.Length;
                //Stream dataStream = request.GetRequestStream();
                //dataStream.Write(byteArray, 0, byteArray.Length);
                //dataStream.Close();
                //WebResponse response = request.GetResponse();
                //dataStream = response.GetResponseStream();
                //StreamReader reader = new StreamReader(dataStream);
                //string responseFromServer = reader.ReadToEnd();

                //HttpClient client = new HttpClient();
                ////client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token.access_token);
                ////client.DefaultRequestHeaders.Add("Accept", "application/json");
                //client.DefaultRequestHeaders.Add("ContentType", "application/x-www-form-urlencoded");
                Uri uri = new Uri("http://52.30.121.166/Backend/api/account/login");
                HttpStringContent content = new HttpStringContent(tresc);
                ////IAsyncOperationWithProgress<HttpResponseMessage, HttpProgress> response = null;
                //HttpResponseMessage response = null;
                //try
                //{
                //    response = await client.PostAsync(uri, content);
                //}
                //catch (WebException)
                //{
                //    return null;
                //}

                var httpClient = new HttpClient();
                httpClient.DefaultRequestHeaders.Accept.Add(new HttpMediaTypeWithQualityHeaderValue("application/json"));
                var wcfResponse = httpClient.PostAsync(uri, content);
                while (wcfResponse.Status != AsyncStatus.Completed)
                {
                    //Czekanie na skończenie Asynchronicznej metody.
                }
                var response = wcfResponse.GetResults().Content.ReadAsStringAsync();
                //Zaczekaj na odpowiedź
                while (response.Status != AsyncStatus.Completed)
                {
                    //Czekanie na skończenie Asynchronicznej metody.
                }

                LoginResponse APIresponse = JsonConvert.DeserializeObject<LoginResponse>(response.GetResults());


                //await Task.Delay(TimeSpan.FromMilliseconds(100));
                //LoginResponse token = Newtonsoft.Json.JsonConvert.DeserializeObject<LoginResponse>(response.GetResults().Content.ReadAsStringAsync().GetResults());
                //return token;
                return APIresponse;
            }
        }
        #endregion Old Code

        /// <summary>
        /// opcjonalne, aby się wylogować należy "zapomnieć" token.
        /// http://52.30.121.166/Backend/Help/Api/POST-api-account-register
        /// </summary>
        public void Logout(LoginResponse token)
        {
            if (ServerAdapter.SampleData)
            {
                //Tutaj zwróć przykładowe dane takie jak w dokumentacji
            }
            else
            {
                //Tutaj zwróć dane z serwra
                token.access_token = "";
            }

            //throw new Exception("Nie implementować!!! Wyrzucić token z pamięci.");
        }

        /// <summary>
        /// Rejestracja
        /// http://52.30.121.166/Backend/Help/Api/POST-api-account-register
        /// </summary>
        /// <param name="requestData">Dane uzytkownika na temat restracji</param>
        /// <returns></returns>
        public bool Register(RegisterRequest requestData)
        {
            if (ServerAdapter.SampleData)
            {
                //Tutaj zwróć przykładowe dane takie jak w dokumentacji
                return true;
            }
            else
            {
                //Najpierw sprawdź czy hasło jest poprawne.
                Utils util = new Utils();
                if (util.WalidujEmail(requestData.Email))
                {
                    if (util.WalidujHaslo(requestData.Password))
                    {
                        Uri uri = new Uri("http://52.30.121.166/Backend/api/account/register");
                        Dictionary<string, object> valuesPOST = new Dictionary<string, object>
                {
                   { "Email", requestData.Email },
                   { "Password", requestData.Password },
                   { "ConfirmPassword", requestData.ConfirmPassword },
                   { "FirstName", requestData.FirstName },
                   { "LastName", requestData.LastName },
                   { "Gender", (int)requestData.Gender },
                   { "Birthdate", string.Format("{0}-{1}-{2}", requestData.BirthDate.Year, requestData.BirthDate.Month, requestData.BirthDate.Day) },
                   { "Description", requestData.Description }
                };
                        string json = JsonConvert.SerializeObject(valuesPOST);
                        HttpClient client = new HttpClient();
                        HttpStringContent content = new HttpStringContent(json, Windows.Storage.Streams.UnicodeEncoding.Utf8, "application/json");
                        var response = client.PostAsync(uri, content);
                        while (response.Status != AsyncStatus.Completed) { /*Czekanie na skończenie Asynchronicznej metody. */ }
                        HttpResponseMessage responseResults = response.GetResults();

                        if (responseResults.StatusCode == Windows.Web.Http.HttpStatusCode.Ok)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }

                }
                else
                {
                    return false;
                }
                
                //this.WalidujHaslo(requestData.Password);
                //this.WalidujEmail(requestData.Email);

            }
        }

        /// <summary>
        /// Sprawdza czy email jest poprawny. Jeśli nie jest to zwraca odpowiedni wyjatek.
        /// </summary>
        /// <param name="email"></param>
        private void WalidujEmail(string email)
        {
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = regex.Match(email);
            if (!match.Success)
                throw new Exception("Email jest niepoprawny.");
        }

        /// <summary>
        /// Sprawdza czy haslo jest poprawne. W przeciwnym wypadku zwraca odpowiedni wyjątek.
        /// </summary>
        /// <param name="password"></param>
        private void WalidujHaslo(string password)
        {
            //Hasło musi miec przynajmniej 6 znaków.
            if (password.Length < 6)
            {
                throw new Exception("Hasło jest zbyt krótkie. Wymagane co najmniej 6 znaków.");
            }

            //Hasło musi zawierać małą literę.
            bool hasSmallLetter = false;
            foreach (char c in password)
            {
                if (char.IsLetter(c) && char.IsLower(c))
                {
                    hasSmallLetter = true;
                    break;
                }
            }
            if (!hasSmallLetter)
            {
                throw new Exception("Hasło musi zawierać co najmniej jedną małą literę.");
            }

            //Hasło musi zawierać dużą literę.
            bool hasBigLetter = false;
            foreach (char c in password)
            {
                if (char.IsLetter(c) && char.IsUpper(c))
                {
                    hasBigLetter = true;
                    break;
                }
            }
            if (!hasBigLetter)
            {
                throw new Exception("Hasło musi zawierać co najmniej jedną dużą literę.");
            }

            //Hasło musi zawierać znak (!,@,#,$,%,^,&,*)
            string requiredChars = "!@#$%^&*.";
            bool hasRequiredChar = false;
            foreach (char c in password)
            {
                foreach (char cc in requiredChars)
                {
                    if (c == cc)
                    {
                        hasRequiredChar = true;
                        break;
                    }
                }
                if (hasRequiredChar) break;
            }
            if (!hasRequiredChar)
            {
                throw new Exception("Hasło musi zawierać co najmniej jeden znak wymagany! Znaki wymagane '" + requiredChars + "'");
            }

            //Czy hasło zawiera niedopuszczalne znaki
            string forbiddenChars = "()";
            bool hasForbiddenChar = false;
            foreach (char c in password)
            {
                foreach (char cc in forbiddenChars)
                {
                    if (c == cc)
                    {
                        hasForbiddenChar = true;
                        break;
                    }
                }
                if (hasForbiddenChar) break;
            }
            if (hasForbiddenChar)
            {
                throw new Exception("Hasło nie może zawierać zabronionych znaków! Znaki zabronione '" + forbiddenChars + "'");
            }
        }

        /// <summary>
        /// ustawienie lokacji
        /// Metoda POST.
        /// http://52.30.121.166/Backend/Help/Api/POST-api-account-location
        /// </summary>
        /// <returns>Czy operacja zakończyła sie powodzeniem.</returns>
        public bool Location(LoginResponse token, Geolocation location)
        {
            if (ServerAdapter.SampleData)
            {
                //Tutaj zwróć przykładowe dane takie jak w dokumentacji
                return true;
            }
            else
            {
                //Tutaj zwróć dane z serwra
                string link = "http://52.30.121.166/Backend/api/account/location";
                var values = new Dictionary<string, object>
                {
                   { "Location", String.Format("{0};{1}",location.Latitude,location.Longitude) },
                };
                string json = Newtonsoft.Json.JsonConvert.SerializeObject(values);
                //string responseJson = string.Empty;

                //using (WebClient client = new WebClient())
                //{
                //    client.Headers[HttpRequestHeader.Accept] = "application/json";
                //    client.Headers[HttpRequestHeader.ContentType] = "application/json";
                //    client.Headers[HttpRequestHeader.Authorization] = "Bearer " + token.access_token;

                //    byte[] response = client.UploadData(apiUrl, Encoding.UTF8.GetBytes(requestJson));

                //    responseJson = Encoding.UTF8.GetString(response);
                //}
                HttpClient client = new HttpClient();
                //client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token.access_token);
                client.DefaultRequestHeaders.Authorization = new Windows.Web.Http.Headers.HttpCredentialsHeaderValue("Bearer", token.access_token);
                //client.DefaultRequestHeaders.Add("Accept", "application/json");
                //client.DefaultRequestHeaders.Add("ContentType", "application/json");
                Uri uri = new Uri(link);
                HttpStringContent content = new HttpStringContent(json,Windows.Storage.Streams.UnicodeEncoding.Utf8, "application/json");
                var response = client.PostAsync(uri, content);
                //while (response.Status != AsyncStatus.Completed)
                //{
                //    //Czekanie na skończenie Asynchronicznej metody.
                //}
                return true;
            }

            //throw new NotImplementedException();
        }

        /// <summary>
        /// Rejestracja użytkownika.
        /// Metoda POST.
        /// http://52.30.121.166/Backend/Help/Api/POST-api-account-register
        /// </summary>
        /// <param name="email"></param>
        /// <returns>Czy operacja zakończyła się powodzeniem.</returns>
        public bool RegisterExternal(string email)
        {
            //Nie komentować!!!

            if (ServerAdapter.SampleData)
            {
                //Tutaj zwróć przykładowe dane takie jak w dokumentacji
                return true;
            }
            else
            {
                //Tutaj zwróć dane z serwra
                return false;
            }

            //throw new NotImplementedException();
        }

        /// <summary>
        /// Zmiana hasła.
        /// http://52.30.121.166/Backend/Help/Api/POST-api-account-changepassword
        /// </summary>
        /// <param name="changePasswordRequest"></param>
        /// <returns></returns>
        public bool ChangePassword(LoginResponse token, ChangePasswordRequest changePasswordRequest)
        {
            if (ServerAdapter.SampleData)
            {
                //Tutaj zwróć przykładowe dane takie jak w dokumentacji
                return true;
            }
            else
            {
                //Tutaj zwróć dane z serwra
                string json = "{";
                string tresc = String.Format("\"OldPassword\":\"{0}\",\"NewPassword\":\"{1}\",\"ConfirmPassword\":\"{2}\"", changePasswordRequest.OldPassword, changePasswordRequest.NewPassword, changePasswordRequest.ConfirmPassword);
                json = json + tresc;
                json = json + "}";
                string link = "http://52.30.121.166/Backend/api/account/changepassword";
                //using (var webClient = new WebClient())
                //{
                //    webClient.Headers.Add(HttpRequestHeader.ContentType, "application/json");
                //    webClient.Headers.Add(HttpRequestHeader.Accept, "application/json");
                //    webClient.Headers.Add(HttpRequestHeader.Authorization, "Bearer " + token.access_token);
                //    var response = webClient.UploadString(link, "POST", json);
                //}
                HttpClient client = new HttpClient();
                //client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token.access_token);
                client.DefaultRequestHeaders.Authorization = new Windows.Web.Http.Headers.HttpCredentialsHeaderValue("Bearer", token.access_token);
                //client.DefaultRequestHeaders.Add("Accept", "application/json");
                //client.DefaultRequestHeaders.Add("ContentType", "application/json");
                Uri uri = new Uri(link);
                HttpStringContent content = new HttpStringContent(json,Windows.Storage.Streams.UnicodeEncoding.Utf8, "application/json");
                var response = client.PostAsync(uri, content);
                //while (response.Status != AsyncStatus.Completed)
                //{
                //    //Czekanie na skończenie Asynchronicznej metody.
                //}
                return true;
            }
            //throw new NotImplementedException();
        }

        public User GetUser(LoginResponse token)
        {
            User user;
            string link = "http://52.30.121.166/Backend/api/account";
            //using (var webClient = new WebClient())
            //{
            //    webClient.Headers.Add(HttpRequestHeader.Authorization, "Bearer " + token.access_token);
            //    var response = webClient.DownloadString(link);
            //    user = Newtonsoft.Json.JsonConvert.DeserializeObject<User>(response);
            //}
            HttpClient client = new HttpClient();
            //client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token.access_token);
            client.DefaultRequestHeaders.Authorization = new Windows.Web.Http.Headers.HttpCredentialsHeaderValue("Bearer", token.access_token);
            Uri uri = new Uri(link);
            var response = client.GetStringAsync(uri);
            while (response.Status != AsyncStatus.Completed)
            {
                //Czekanie na skończenie Asynchronicznej metody.
            }
            user = Newtonsoft.Json.JsonConvert.DeserializeObject<User>(response.GetResults());
            return user;
        }

        public void SetProfilePhoto(LoginResponse token, int id)
        {
            string link = "http://52.30.121.166/Backend/api/account/profilephoto/" + id;
            //using (var webClient = new WebClient())
            //{
            //    webClient.Headers.Add(HttpRequestHeader.Authorization, "Bearer " + token.access_token);
            //    var response = webClient.DownloadString(link);
            //}
            HttpClient client = new HttpClient();
            //client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token.access_token);
            client.DefaultRequestHeaders.Authorization = new Windows.Web.Http.Headers.HttpCredentialsHeaderValue("Bearer", token.access_token);
            Uri uri = new Uri(link);
            HttpStringContent content = new HttpStringContent("");
            var response = client.PostAsync(uri,content);
            //while (response.Status != AsyncStatus.Completed)
            //{
            //    //Czekanie na skończenie Asynchronicznej metody.
            //}
        }

        public void SetDescription(LoginResponse token, string description)
        {
            string link = "http://52.30.121.166/Backend/api/account/description";
            string json = "{" + String.Format("\"Description\":\"{0}\"", description) + "}";
            //using (var client = new WebClient())
            //{
            //    client.Headers.Add(HttpRequestHeader.Authorization, "Bearer " + token.access_token);
            //    client.Headers.Add(HttpRequestHeader.Accept, "application/json");
            //    client.Headers.Add(HttpRequestHeader.ContentType, "application/json");
            //    var response = client.UploadString(link, "POST", json);
            //}
            HttpClient client = new HttpClient();
            //client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token.access_token);
            client.DefaultRequestHeaders.Authorization = new Windows.Web.Http.Headers.HttpCredentialsHeaderValue("Bearer", token.access_token);
            //client.DefaultRequestHeaders.Add("Accept", "application/json");
            //client.DefaultRequestHeaders.Add("ContentType", "application/json");
            Uri uri = new Uri(link);
            HttpStringContent content = new HttpStringContent(json,Windows.Storage.Streams.UnicodeEncoding.Utf8, "application/json");
            var response = client.PostAsync(uri, content);
            //while (response.Status != AsyncStatus.Completed)
            //{
            //    //Czekanie na skończenie Asynchronicznej metody.
            //}
        }
    }
}
