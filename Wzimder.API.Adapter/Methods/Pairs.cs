﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wzimder.API.Adapter.Interfaces;
using Wzimder.Model.Types;
using Windows.Web.Http;
using Wzimder.API.Adapter.Models;
using Windows.Foundation;

namespace Wzimder.API.Adapter.Methods
{
    internal class Pairs : IPairs
    {
        /// <summary>
        /// Pobranie par użytkownika.
        /// Metoda GET.
        /// http://52.30.121.166/Backend/Help/Api/GET-api-pairs
        /// </summary>
        /// <returns>Lista wsyzstkich par.</returns>
        public List<User> GetPairs(LoginResponse token)
        {
            List<User> listaUsers;
            if (ServerAdapter.SampleData)
            {
                //Tutaj zwróć przykładowe dane takie jak w dokumentacji
                //listaPar = new List<Pair> { };
                //Pair para = new Pair();
                //para.FirstUserId = 1;
                //para.FirstUserStatus = UserStatus.Like;
                //para.Id = 1;
                //para.SecondUserId = 2;
                //para.SecondUserStatus = UserStatus.Like;
                //listaPar.Add(para);
                //Pair para2 = new Pair();
                //para2.FirstUserId = 1;
                //para2.FirstUserStatus = UserStatus.Like;
                //para2.Id = 2;
                //para2.SecondUserId = 3;
                //para2.SecondUserStatus = UserStatus.DontLike;
                //listaPar.Add(para2);
                listaUsers = new List<User>() { };
            }
            else
            {
                //Tutaj zwróć dane z serwra
                string link = "http://52.30.121.166/Backend/api/pairs";
                Uri uri = new Uri(link);
                HttpClient client = new HttpClient();
                //client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token.access_token);
                client.DefaultRequestHeaders.Authorization = new Windows.Web.Http.Headers.HttpCredentialsHeaderValue("Bearer", token.access_token);
                IAsyncOperationWithProgress<string, HttpProgress> responseTask = client.GetStringAsync(uri);
                while (responseTask.Status != AsyncStatus.Completed && responseTask.Status != AsyncStatus.Error) { /*Czekanie na skończenie Asynchronicznej metody.*/ }
                var response = responseTask.GetResults();
                listaUsers = Newtonsoft.Json.JsonConvert.DeserializeObject<List<User>>(response);
            }

            //throw new NotImplementedException();
            return listaUsers;
        }

        /// <summary>
        /// Polubienie/Odrzucenie innego użytkownika.
        /// Metoda POST.
        /// http://52.30.121.166/Backend/Help/Api/POST-api-pairs
        /// </summary>
        /// <param name="newPair">Nowa para.</param>
        /// <returns>Czy operacja zakończyła się powodzeniem.</returns>
        public bool SendPairs(LoginResponse token, Pair newPair)
        {
            if (ServerAdapter.SampleData)
            {
                //Tutaj zwróć przykładowe dane takie jak w dokumentacji
                return true;
            }
            else
            {
                //Tutaj zwróć dane z serwra
                string link = "http://52.30.121.166/Backend/api/pairs";
                string json = "{" + String.Format("\"UserId\":\"{0}\",\"Status\":\"{1}\"", newPair.FirstUserId, newPair.FirstUserStatus) + "}";
                HttpClient client = new HttpClient();
                //client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token.access_token);
                client.DefaultRequestHeaders.Authorization = new Windows.Web.Http.Headers.HttpCredentialsHeaderValue("Bearer", token.access_token);
                //client.DefaultRequestHeaders.Add("Accept", "application/json");
                //client.DefaultRequestHeaders.Add("ContentType", "application/json");
                Uri uri = new Uri(link);
                HttpStringContent content = new HttpStringContent(json,Windows.Storage.Streams.UnicodeEncoding.Utf8, "application/json");
                var response = client.PostAsync(uri, content);
                //while (response.Status != AsyncStatus.Completed)
                //{
                //    //Czekanie na skończenie Asynchronicznej metody.
                //}
                return true;
            }

            //throw new NotImplementedException();
        }
    }
}
