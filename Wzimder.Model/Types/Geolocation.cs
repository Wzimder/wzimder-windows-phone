﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wzimder.Model.Types
{
    public class Geolocation
    {
        /// <summary>
        /// Długość geograficzna.
        /// </summary>
        public double Longitude { get; set; }
        /// <summary>
        /// Szerokośc geograficzna.
        /// </summary>
        public double Latitude { get; set; }

        public override string ToString()
        {
            //TODO:
            throw new NotImplementedException();
        }
    }
}
