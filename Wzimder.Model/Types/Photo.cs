﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wzimder.Model.Types
{
    public class Photo
    {
        /// <summary>
        /// Photo Id.
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Photo url.
        /// </summary>
        public Uri PhotoUrl { get; set; }
        /// <summary>
        /// Photo UserId, czyli do kogo należy zdjęcie.
        /// </summary>
        public int UserId { get; set; }

        public override string ToString()
        {
            //TODO:
            throw new NotImplementedException();
        }
    }
}
