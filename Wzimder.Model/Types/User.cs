﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wzimder.Model.Types
{
    /// <summary>
    /// Użytkownik i jego pełne informacje.
    /// </summary>
    public class User
    {
        /// <summary>
        /// Id usera.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Nazwa użytkownika.
        /// </summary>
        public string Username { get; set; }
        /// <summary>
        /// Imię usera.
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// Nazwisko usera.
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// Imie i Nazwisko.
        /// </summary>
        public string FirstNameAndLastName { get { return FirstName + " " + LastName; } }
        /// <summary>
        /// Email usera.
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// Płeć usera.
        /// </summary>
        public Gender Gender { get; set; }
        /// <summary>
        /// Data urodzenia usera.
        /// </summary>
        public DateTime BirthDate { get; set; }
        /// <summary>
        /// Wiek usera.
        /// </summary>
        public int Age { get { return DateTime.Now.Year - BirthDate.Year;  } }
        /// <summary>
        /// Opis usera.
        /// </summary>
        public string Description { get; set; }

        //----------------------------------------------------------------------

        /// <summary>
        /// Obecna lokalizacja usera.
        /// </summary>
        public Geolocation Geolocation { get; set; }

        /// <summary>
        /// Lista otrzymanych wiadomości od innych.
        /// </summary>
        public List<Message> MessagesReceived { get; set; }
        /// <summary>
        /// Lista wiadomości wysłanych.
        /// </summary>
        public List<Message> MessageSent { get; set; }

        /// <summary>
        /// Wszystkie zdjęcia usera.
        /// </summary>
        public List<Photo> Photos { get; set; }

        /// <summary>
        /// Id zdjęcia.
        /// </summary>
        public int ProfilePhotoId { get; set; }
        public int Photo { get { return ProfilePhotoId; } set { ProfilePhotoId = value; } }
        /// <summary>
        /// Obiekt zdjęcia.
        /// </summary>
        public Photo PhotoObject { get; set; }
        /// <summary>
        /// Link do zdjecia profilowego na serwerze.
        /// </summary>
        public string PhotoLink
        {
            get
            {
                if (ProfilePhotoId == 0)
                {
                    return "ms-appx:///Assets/avatar.jpg";
                }

                return "http://52.30.121.166/Backend/api/photos/" + this.ProfilePhotoId;
            }
        }

        /// <summary>
        /// Główne zdjęcie użytkownika
        /// </summary>
        public SearchOption SearchOption { get; set; }


        public override string ToString()
        {
            //TODO:
            throw new NotImplementedException();
        }
    }

    public enum Gender
    {
        Male = 0,
        Female = 1,
        Unknown = 2,
    }
}
