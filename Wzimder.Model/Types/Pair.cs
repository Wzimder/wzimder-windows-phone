﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wzimder.Model.Types
{
    public class Pair
    {
        /// <summary>
        /// Id pary użwytkowników.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Id pierwszego użwykownika należacego do pary.
        /// </summary>
        public string FirstUserId { get; set; }
        /// <summary>
        /// Id drugiego uzytkownika należącego do pary.
        /// </summary>
        public string SecondUserId { get; set; }

        /// <summary>
        /// Status pierwszego usera.
        /// </summary>
        public UserStatus FirstUserStatus { get; set; }
        /// <summary>
        /// Status drugiego usera.
        /// </summary>
        public UserStatus SecondUserStatus { get; set; }

        public override string ToString()
        {
            //TODO:
            throw new NotImplementedException();
        }
    }

    public enum UserStatus
    {
        Like = 0,
        DontLike = 1,	
        Unknown = 2,
    }
}
