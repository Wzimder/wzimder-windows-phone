﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wzimder.Model.Types
{
    public class Message
    {
        /// <summary>
        /// Message Id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Treść wiadomości.
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// User wysyłający wiadomośc.
        /// </summary>
        public User Sender { get; set; }
        /// <summary>
        /// User odbierajacy wiadomość.
        /// </summary>
        public User Receiver { get; set; }

        /// <summary>
        /// Czy wiadomość została przeczytana.
        /// </summary>
        public bool Unread { get; set; }

        public override string ToString()
        {
            //TODO:
            throw new NotImplementedException();
        }
    }
}
