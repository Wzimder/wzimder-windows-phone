﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wzimder.Model.Types
{
    public class SearchOption
    {
        /// <summary>
        /// Id ustawień wyszukiwania.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Id usera do którego należą ustawienia.
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Minimalny wiek - kryterium wyszukiwania.
        /// </summary>
        public int MinAge { get; set; }
        /// <summary>
        /// Maksymalny wiek - kryterium wyszukiwania.
        /// </summary>
        public int MaxAge { get; set; }
        /// <summary>
        /// Płeć - kryterium wyszukiwania.
        /// </summary>
        public Gender Gender { get; set; }
        /// <summary>
        /// Maksymalny dystans od drugiego usera - kryterium wyszukiwania.
        /// </summary>
        public int MaxDistanceKM { get; set; }

        public override string ToString()
        {
            //TODO:
            throw new NotImplementedException();
        }
    }
}
